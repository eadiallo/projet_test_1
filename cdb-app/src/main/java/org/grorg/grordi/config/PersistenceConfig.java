package org.grorg.grordi.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.*;

import javax.sql.DataSource;

@Configuration
@EnableAutoConfiguration
@EntityScan("org.grorg.grordi.entity")
@ComponentScan("org.grorg.grordi.dao")
public class PersistenceConfig {
    /**
     * Setup datasource depending on spring datasource hirkari properties.
     * @return The configured datasource
     */
    @Bean
    @Primary
    @ConfigurationProperties(prefix = "spring.datasource.hikari")
    public DataSource dataSource() {
        return DataSourceBuilder.create().build();
    }
}
