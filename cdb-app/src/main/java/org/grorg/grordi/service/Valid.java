package org.grorg.grordi.service;

/**
 * Class that represent a type that has been validated.
 * @param <T> The validated type
 */
public final class Valid<T> extends AbstractValid<T> {
    /**
     * Validates the element given in parameter.
     * @param validElement The element to validate
     */
    Valid(T validElement) {
        super(validElement);
    }
}
