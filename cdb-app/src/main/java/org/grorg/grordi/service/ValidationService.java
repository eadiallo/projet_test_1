package org.grorg.grordi.service;

import org.grorg.grordi.dto.ComputerWithCompanyIdDto;
import org.grorg.grordi.dto.ComputerWithCompanyIdDtoFactory;
import org.grorg.grordi.dto.ComputerWithCompanyIdDtoValidator;
import org.grorg.grordi.dto.SearchCriteria;
import org.grorg.grordi.dto.error.Errors;
import org.grorg.grordi.dto.error.Error;
import org.grorg.grordi.entity.Company;
import org.grorg.grordi.entity.Computer;
import io.vavr.control.Either;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

@Service
public final class ValidationService {
    private ComputerService computerService;
    private CompanyService companyService;

    /**
     * Creates new validation service depending on CompanyService.
     * @param companyService CompanyService used to get computer company information
     * @param computerService ComputerService used to get computer information
     */
    public ValidationService(ComputerService computerService, CompanyService companyService) {
        this.computerService = computerService;
        this.companyService = companyService;
    }

    /**
     * Validate a computer dto for computer persisting operation and return validated computer for creation if
     * validation tests are ok, an error list otherwise.
     *
     * @param computer The computer to validate
     * @return Either the errors list if there are some (left), either the validated computer if the validation passed
     * (right)
     */
    public Either<List<Error>, ValidForCreation<Computer>> validateComputerWithCompanyIdDtoForCreation(
            ComputerWithCompanyIdDto computer) {
        List<Error> errors = ComputerWithCompanyIdDtoValidator.validateForCreation(computer);
        Company company = null;
        if (computer != null) {
            company = companyService.findCompanyById(computer.getCompany());
            if (company == null) {
                errors.add(Errors.COMPUTER_SHOULD_HAVE_VALID_COMPANY);
            }
        }

        if (!errors.isEmpty()) {
            return Either.left(errors);
        } else {
            return Either.right(new ValidForCreation<>(fromDtoToComputer(computer, company)));
        }
    }

    /**
     * Validate a computer with company id dto for an update operation.
     * @param computerDto The computer dto used to update the associate computer
     * @return Either a list of error if there are some, either a validated computer
     */
    public Either<List<Error>, ValidForUpdate<Computer>> validateComputerWithCompanyIdDtoForUpdate(
            ComputerWithCompanyIdDto computerDto) {
        Computer computer = computerService.findComputerById(computerDto.getId());
        if (computer == null) {
            return Either.left(Collections.singletonList(Errors.COMPUTER_NOT_FOUND));
        }

        List<Error> errors = ComputerWithCompanyIdDtoValidator.validate(computerDto);
        if (computer.getCompany() == null && computerDto.getCompany() != null ||
                computer.getCompany() != null && !computer.getCompany().getId().equals(computerDto.getCompany())) {
            errors.add(Errors.COMPUTER_COMPANY_FIELD_CANNOT_BE_UPDATE);
        }

        if (!errors.isEmpty()) {
            return Either.left(errors);
        } else {
            return Either.right(new ValidForUpdate<>(fromDtoToComputer(computerDto, computer.getCompany())));
        }
    }

    /**
     * Validate a search criteria. If the validation succeed return a valid search criteria otherwise return an error
     * list.
     * @param criteria The search criteria to validate
     * @param <T> The search criteria target class
     * @return Either the errors list if there are some (left), either the validated search criteria
     */
    public <T> Either<List<Error>, Valid<SearchCriteria<T>>> validateSearchCriteria(SearchCriteria<T> criteria) {
        List<Error> errors = new ArrayList<>();

        // this condition is to ensure that the sorting exists and to avoid sql injection
        if (!criteria.getSortingColumn().isEmpty() &&
                getTargetClassFieldNames(criteria.getTargetClass()).noneMatch(criteria.getSortingColumn()::equals)) {
            errors.add(Errors.SEARCH_CRITERIA_SORTING_COLUMN_SHOULD_MATCH_WITH_COMPUTER_FIELDS);
        }
        if (criteria.getPageIndex() < 0) {
            errors.add(Errors.SEARCH_CRITERIA_PAGE_INDEX_SHOULD_BE_HIGHER_OR_EQUAL_TO_0);
        }
        if (criteria.getPageSize() < 1) {
            errors.add(Errors.SEARCH_CRITERIA_PAGE_SIZE_SHOULD_BE_HIGHER_THAN_0);
        }
        if (criteria.getPageSize() > 100) {
            errors.add(Errors.SEARCH_CRITERIA_PAGE_SIZE_SHOULD_NOT_BE_HIGHER_THAN_100);
        }

        if (!errors.isEmpty()) {
            return Either.left(errors);
        } else {
            return Either.right(new Valid<>(criteria));
        }
    }

    /**
     * Returns the target class given in parameter field's names.
     * @param targetClass The target class to extract field's names
     * @param <T> The target class type
     * @return The field's names of the target class
     */
    private <T> Stream<String> getTargetClassFieldNames(Class<T> targetClass) {
        return Arrays.stream(targetClass.getDeclaredFields()).map(Field::getName);
    }

    /**
     * Transform a computer dto in computer. The company information is given in parameter
     * @param dto The dto to transform
     * @param company The computer company
     * @return The transformed dto in computer
     */
    private Computer fromDtoToComputer(ComputerWithCompanyIdDto dto, Company company) {
        Computer computer = ComputerWithCompanyIdDtoFactory.fromDtoToComputer(dto);
        computer.setCompany(company);
        return computer;
    }
}
