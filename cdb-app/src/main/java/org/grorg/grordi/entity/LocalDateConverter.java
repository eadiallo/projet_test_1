package org.grorg.grordi.entity;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.Optional;

/**
 * Convert LocalDate to sql.Date since time zone information are automatically added to converted local date on persist
 * that typically took of a day to local date in UTC+1.
 */
@Converter(autoApply = true)
public class LocalDateConverter implements AttributeConverter<LocalDate, Date> {
    @Override
    public Date convertToDatabaseColumn(LocalDate date) {
        // We must convert LocalDate to LocalDateTime with UTC Zone offset instead of just Date.valueOf(date)
        // because Date.valueOf consider local time zone on conversion.
        // That add wrong hour to the date and if we send this converted date to database we could store the wrong date
        // typically the date minus one day.
        return Optional.ofNullable(date).map(d ->
                new Date(LocalDateTime.of(d, LocalTime.MIN).toInstant(ZoneOffset.UTC).toEpochMilli())
        ).orElse(null);
    }

    @Override
    public LocalDate convertToEntityAttribute(Date date) {
        return Optional.ofNullable(date).map(Date::toLocalDate).orElse(null);
    }
}
