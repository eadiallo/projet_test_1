package org.grorg.grordi.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "operation")
public final class Operation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Enumerated(EnumType.STRING)
    private Entity entity;
    @Enumerated(EnumType.STRING)
    private Type type;
    private long entityId;

    public enum Type {
        INSERTION,
        UPDATING,
        DELETION
    }

    public enum Entity {
        COMPUTER,
        COMPANY
    }

    /**
     * Empty constructor for serialization.
     */
    public Operation() { }

    /**
     * Creates an Operation from builder.
     * @param builder The builder that contains operation parameters
     */
    private Operation(Builder builder) {
        this.id = builder.id;
        this.entity = builder.entity;
        this.type = builder.type;
        this.entityId = builder.entityId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Entity getEntity() {
        return entity;
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public long getEntityId() {
        return entityId;
    }

    public void setEntityId(long entityId) {
        this.entityId = entityId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Operation operation = (Operation) o;
        return entityId == operation.entityId &&
                Objects.equals(id, operation.id) &&
                entity == operation.entity &&
                type == operation.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public static class Builder {
        private Long id;
        private Entity entity;
        private Type type;
        private long entityId;

        /**
         * Forbids direct builder creation.
         */
        private Builder() { }

        /**
         * Add the operation id to the builder and return it.
         * @param id The operation id
         * @return The current builder
         */
        public Builder withId(long id) {
            this.id = id;
            return this;
        }

        /**
         * Add COMPUTER entity to the builder and return it.
         * @return The current builder
         */
        public Builder forComputer() {
            this.entity = Entity.COMPUTER;
            return this;
        }

        /**
         * Add COMPANY entity to the builder and return it.
         * @return The current builder
         */
        public Builder forCompany() {
            this.entity = Entity.COMPANY;
            return this;
        }

        /**
         * Add INSERTION type to the builder and return it.
         * @return The current builder
         */
        public Builder ofInsertionType() {
            this.type = Type.INSERTION;
            return this;
        }

        /**
         * Add UPDATING type to the builder and return it.
         * @return The current builder
         */
        public Builder ofUpdatingType() {
            this.type = Type.UPDATING;
            return this;
        }

        /**
         * Add DELETION type to the builder and return it.
         * @return The current builder
         */
        public Builder ofDeletionType() {
            this.type = Type.DELETION;
            return this;
        }

        /**
         * Add the entity id to the builder and return it.
         * @param entityId The entity id
         * @return The current builder
         */
        public Builder withEntityId(long entityId) {
            this.entityId = entityId;
            return this;
        }

        /**
         * Creates a new Operation with previous given parameters.
         * @return A new configured operation
         */
        public Operation build() {
            return new Operation(this);
        }
    }

    /**
     * Creates a new operation builder.
     * @return A new builder
     */
    public static Builder builder() {
        return new Builder();
    }
}
