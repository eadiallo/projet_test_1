package org.grorg.grordi.entity;

public enum Role {
    USER,
    ADMIN
}
