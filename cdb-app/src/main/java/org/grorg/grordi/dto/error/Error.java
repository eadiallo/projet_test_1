package org.grorg.grordi.dto.error;

public interface Error {
    /**
     * Return error code as string.
     * @return The error code
     */
    String getCode();

    /**
     * Return error message as string.
     * @return The error messagex
     */
    String getMessage();
}
