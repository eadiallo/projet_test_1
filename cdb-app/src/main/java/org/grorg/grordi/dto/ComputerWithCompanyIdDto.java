package org.grorg.grordi.dto;

import java.util.Objects;

public final class ComputerWithCompanyIdDto extends ComputerDto {
    private Long company;

    public Long getCompany() {
        return company;
    }

    public void setCompany(Long company) {
        this.company = company;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        ComputerWithCompanyIdDto that = (ComputerWithCompanyIdDto) o;
        return Objects.equals(company, that.company);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return "ComputerWithCompanyIdDto{" +
                "id=" + getId() +
                ", name='" + getName() + '\'' +
                ", introduced='" + getIntroduced() + '\'' +
                ", discontinued='" + getDiscontinued() + '\'' +
                ", company=" + company +
                '}';
    }
}
