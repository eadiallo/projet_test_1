package org.grorg.grordi.dto;

import java.util.Collection;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

public final class Page<T> {
    private int pageIndex;
    private int pageSize;
    private String search;
    private SortCriteria sort;
    private int totalPage;
    private long totalElements;
    private Collection<T> elements;

    /**
     * Default constructor for serialisation.
     */
    public Page() { }

    /**
     * Constructor for building a page from a builder instance.
     * @param pageBuilder The builder instance
     */
    private Page(Builder<T> pageBuilder) {
        this.pageIndex = pageBuilder.pageIndex;
        this.pageSize = pageBuilder.pageSize;
        this.search = pageBuilder.search;
        this.sort = new SortCriteria(pageBuilder.column, pageBuilder.orientation);
        this.totalPage = pageBuilder.totalPage;
        this.totalElements = pageBuilder.totalElements;
        this.elements = pageBuilder.elements;
    }

    public static final class SortCriteria {
        private String column;
        private String orientation;

        /**
         * Default constructor for serialisation.
         */
        SortCriteria() { }

        /**
         * Create a sort criteria with column and orientation information.
         * @param column The sort criteria column
         * @param orientation The sort criteria orientation
         */
        SortCriteria(String column, String orientation) {
            this.column = column;
            this.orientation = orientation;
        }

        public String getColumn() {
            return column;
        }

        public String getOrientation() {
            return orientation;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            SortCriteria that = (SortCriteria) o;
            return Objects.equals(column, that.column) &&
                    Objects.equals(orientation, that.orientation);
        }

        @Override
        public int hashCode() {
            return Objects.hash(column, orientation);
        }

        @Override
        public String toString() {
            return "SortCriteria{" +
                    "column='" + column + '\'' +
                    ", orientation='" + orientation + '\'' +
                    '}';
        }
    }

    public static class Builder<T> {
        private int pageIndex;
        private int pageSize;
        private String search;
        private String column;
        private String orientation;
        private int totalPage;
        private long totalElements;
        private Collection<T> elements;

        /**
         * Forbids the builder construction from here.
         */
        private Builder() { }

        /**
         * Sets the pageIndex and returns the builder.
         * @param pageIndex The page pageIndex
         * @return The current builder
         */
        public Builder<T> pageIndex(int pageIndex) {
            this.pageIndex = pageIndex;
            return this;
        }

        /**
         * Sets the pageSize and returns the builder.
         * @param pageSize The page pageSize
         * @return The current builder
         */
        public Builder<T> pageSize(int pageSize) {
            this.pageSize = pageSize;
            return this;
        }

        /**
         * Sets the search and returns the builder.
         * @param search The page search
         * @return The current builder
         */
        public Builder<T> search(String search) {
            this.search = search;
            return this;
        }

        /**
         * Sets the column and returns the builder.
         * @param column The page column
         * @return The current builder
         */
        public Builder<T> column(String column) {
            this.column = column;
            return this;
        }

        /**
         * Sets the orientation and returns the builder.
         * @param orientation The page orientation
         * @return The current builder
         */
        public Builder<T> orientation(String orientation) {
            this.orientation = orientation;
            return this;
        }

        /**
         * Sets the totalPage and returns the builder.
         * @param totalPage The page totalPage
         * @return The current builder
         */
        public Builder<T> totalPage(int totalPage) {
            this.totalPage = totalPage;
            return this;
        }

        /**
         * Sets the totalElements and returns the builder.
         * @param totalElements The page totalElements
         * @return The current builder
         */
        public Builder<T> totalElements(long totalElements) {
            this.totalElements = totalElements;
            return this;
        }

        /**
         * Sets the elements and returns the builder.
         * @param elements The page elements
         * @return The current builder
         */
        public Builder<T> elements(Collection<T> elements) {
            this.elements = elements;
            return this;
        }

        /**
         * Build the page.
         * @return The built page
         */
        public Page<T> build() {
            return new Page<>(this);
        }
    }

    /**
     * Returns a new builder instance.
     * @param <T> The page to build generic type
     * @return A new builder instance
     */
    public static <T> Builder<T> builder() {
        return new Builder<>();
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public int getPageSize() {
        return pageSize;
    }

    public String getSearch() {
        return search;
    }

    public SortCriteria getSort() {
        return sort;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public long getTotalElements() {
        return totalElements;
    }

    public Collection<T> getElements() {
        return elements;
    }

    /**
     * Create a new page with search criteria information and result.
     * @param criteria The base search criteria
     * @param elements The found elements
     * @param totalElements The whole number of elements in database
     * @param <T> The page elements type
     * @return A new built page
     */
    public static <T> Page<T> fromSearchCriteriaAndResult(SearchCriteria criteria, Collection<T> elements,
                                                          long totalElements) {
        return new Builder<T>()
                .pageIndex(criteria.getPageIndex())
                .pageSize(criteria.getPageSize())
                .search(criteria.getSearch().substring(1, criteria.getSearch().length() - 1))
                .column(criteria.getSortingColumn())
                .orientation(criteria.getSortingOrientation().getValue())
                .totalPage((int) Math.ceil(((double) totalElements) / criteria.getPageSize()))
                .totalElements(totalElements)
                .elements(elements)
                .build();
    }

    /**
     * Transform page elements into new elements depending on mapper given in parameter.
     * @param elementsMapper The mapper used to transform elements
     * @param <R> The mapper return type
     * @return A new page with mapped elements
     */
    public <R> Page<R> mapElements(Function<T, R> elementsMapper) {
        return new Builder<R>()
                .pageIndex(pageIndex)
                .pageSize(pageSize)
                .search(search)
                .column(sort.column)
                .orientation(sort.orientation)
                .totalPage(totalPage)
                .totalElements(totalElements)
                .elements(elements.stream().map(elementsMapper).collect(Collectors.toList()))
                .build();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Page<?> page = (Page<?>) o;
        return pageIndex == page.pageIndex &&
                pageSize == page.pageSize &&
                totalPage == page.totalPage &&
                totalElements == page.totalElements &&
                Objects.equals(search, page.search) &&
                Objects.equals(sort, page.sort) &&
                Objects.equals(elements, page.elements);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pageIndex, pageSize, search, sort, totalPage, totalElements, elements);
    }

    @Override
    public String toString() {
        return "Page{" +
                "pageIndex=" + pageIndex +
                ", pageSize=" + pageSize +
                ", search='" + search + '\'' +
                ", sort=" + sort +
                ", totalPage=" + totalPage +
                ", totalElements=" + totalElements +
                ", elements=" + elements +
                '}';
    }
}
