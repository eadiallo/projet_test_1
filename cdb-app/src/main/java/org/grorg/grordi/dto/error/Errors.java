package org.grorg.grordi.dto.error;

public final class Errors {
    /**
     * Error is an helper static class and should not be instantiate.
     * @throws IllegalAccessException When we try to instantiate Errors via reflexion
     */
    private Errors() throws IllegalAccessException {
        throw new IllegalAccessException("Error should not be instantiate");
    }

    public static final Error COMPUTER_NOT_FOUND = new ErrorImpl(
            "computer.not.found",
            "The computer you are looking for does not exist");

    public static final Error COMPANY_NOT_FOUND = new ErrorImpl(
            "company.not.found",
            "The company you are looking for does not exist");

    public static final Error COMPUTER_SHOULD_NOT_BE_EMPTY = new ErrorImpl(
            "computer.should.not.be.empty",
            "The computer you give in parameter is null and should not");

    public static final Error COMPUTER_NAME_SHOULD_NOT_BE_EMPTY = new ErrorImpl(
            "computer.name.should.not.be.empty",
            "The computer name should not have a null or empty name");

    public static final Error COMPUTER_INTRODUCED_DATE_SHOULD_BE_IN_ISO_FORMAT = new ErrorImpl(
            "computer.introduced.date.should.be.in.iso.format",
            "The computer introduction date should be in ISO format");

    public static final Error COMPUTER_DISCONTINUED_DATE_SHOULD_BE_IN_ISO_FORMAT = new ErrorImpl(
            "computer.discontinued.date.should.be.in.iso.format",
            "The computer discontinuation date should be in ISO format");

    public static final Error COMPUTER_INTRODUCED_DATE_SHOULD_BE_BEFORE_DISCONTINUED_DATE = new ErrorImpl(
            "computer.introduced.date.should.be.before.discontinued.date",
            "The computer introduction date should be later than discontinued date");

    public static final Error COMPUTER_TO_CREATE_SHOULD_NOT_HAVE_ID = new ErrorImpl(
            "computer.to.create.should.not.have.id",
            "The computer to create should not have an id");

    public static final Error COMPUTER_SHOULD_HAVE_VALID_COMPANY = new ErrorImpl(
            "computer.should.have.valid.company",
            "The computer should be associated with an existing company");

    public static final Error SEARCH_CRITERIA_SORTING_COLUMN_SHOULD_MATCH_WITH_COMPUTER_FIELDS = new ErrorImpl(
            "search.criteria.sorting.column.should.match.with.computer.fields",
            "The search criteria indicates a base field to sort that does not exist in computer");

    public static final Error SEARCH_CRITERIA_PAGE_SIZE_SHOULD_NOT_BE_HIGHER_THAN_100 = new ErrorImpl(
            "search.criteria.page.size.should.not.be.higher.than.100",
            "The search criteria indicates page size higher than 100 elements, that is not reasonable");

    public static final Error SEARCH_CRITERIA_PAGE_SIZE_SHOULD_BE_HIGHER_THAN_0 = new ErrorImpl(
            "search.criteria.page.size.should.be.higher.than.0",
            "The search criteria page size should be higher than 0");

    public static final Error SEARCH_CRITERIA_PAGE_INDEX_SHOULD_BE_HIGHER_OR_EQUAL_TO_0 = new ErrorImpl(
            "search.criteria.page.index.should.be.higher.or.equal.to.0",
            "The search criteria page index should be higher or equal to 0");

    public static final Error COMPUTER_COMPANY_FIELD_CANNOT_BE_UPDATE = new ErrorImpl(
            "computer.company.field.cannot.be.update",
            "The computer company field cannot be updated");
}
