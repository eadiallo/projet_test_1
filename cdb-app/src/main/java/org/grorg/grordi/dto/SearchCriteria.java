package org.grorg.grordi.dto;

import java.util.Objects;

public final class SearchCriteria<T> {
    public static final String SORTING_ORIENTATION_ASCENDING = "ASC";
    public static final String SORTING_ORIENTATION_DESCENDING = "DESC";

    public static final int DEFAULT_PAGE_INDEX = 0;
    public static final int DEFAULT_PAGE_SIZE = 20;
    public static final String DEFAULT_SEARCH = "";
    public static final String DEFAULT_SORTING_COLUMN = "";
    public static final SortingOrientation DEFAULT_SORTING_ORIENTATION = SortingOrientation.ASCENDING;
    public static final String DEFAULT_SORTING_ORIENTATION_STRING = SORTING_ORIENTATION_ASCENDING;

    private final Class<T> targetClass;
    private final int pageIndex;
    private final int pageSize;
    private final String search;
    private final String sortingColumn;
    private final SortingOrientation sortingOrientation;

    /**
     * Create new SearchCriteria from a builder.
     * @param builder The builder that contains a building information
     */
    private SearchCriteria(Builder<T> builder) {
        this.targetClass = builder.targetClass;
        this.pageIndex = builder.pageIndex;
        this.pageSize = builder.pageSize;
        this.search = builder.search;
        this.sortingColumn = builder.sortingColumn;
        this.sortingOrientation = builder.sortingOrientation;
    }

    /**
     * Create a new SearchCriteria with default arguments that is to say:
     * <ul>
     *     <li>SearchCriteria.DEFAULT_PAGE_INDEX</li>
     *     <li>SearchCriteria.DEFAULT_PAGE_SIZE</li>
     *     <li>SearchCriteria.DEFAULT_SEARCH</li>
     *     <li>SearchCriteria.DEFAULT_SORTING_COLUMN</li>
     *     <li>SearchCriteria.DEFAULT_SORTING_ORIENTATION</li>
     * </ul>.
     * @param targetClass The target class for the search
     * @param <T> The type of the target class
     * @return A new search criteria with default criteria
     */
    public static <T> SearchCriteria<T> defaultCriteria(Class<T> targetClass) {
        return new SearchCriteria<>(new Builder<>(targetClass));
    }

    /**
     * Creates new builder for search criteria and returns it.
     * @param targetClass The target class for the search
     * @param <T> The type of the target class
     * @return A new SearchCriteria.Builder
     */
    public static <T> Builder<T> builder(Class<T> targetClass) {
        return new Builder<>(targetClass);
    }

    /**
     * A builder for SearchCriteria.
     */
    public static class Builder<T> {
        private Class<T> targetClass;
        private int pageIndex = DEFAULT_PAGE_INDEX;
        private int pageSize = DEFAULT_PAGE_SIZE;
        private String search = DEFAULT_SEARCH;
        private String sortingColumn = DEFAULT_SORTING_COLUMN;
        private SortingOrientation sortingOrientation = DEFAULT_SORTING_ORIENTATION;

        /**
         * Creates a new builder for SearchCriteria for given target class.
         * @param targetClass The target class for the research
         */
        private Builder(Class<T> targetClass) {
            Objects.requireNonNull(targetClass);
            this.targetClass = targetClass;
        }

        /**
         * Sets pageIndex and return builder.
         * @param pageIndex The pageIndex value to set
         * @return The current builder
         */
        public Builder<T> pageIndex(int pageIndex) {
            this.pageIndex = pageIndex;
            return this;
        }

        /**
         * Sets pageSize and return builder.
         * @param pageSize The pageSize value to set
         * @return The current builder
         */
        public Builder<T> pageSize(int pageSize) {
            this.pageSize = pageSize;
            return this;
        }

        /**
         * Sets search and return builder. If the search given in parameter is null, set search to empty string.
         * @param search The search value to set
         * @return The current builder
         */
        public Builder<T> search(String search) {
            if (search != null) {
                this.search = search;
            }
            return this;
        }

        /**
         * Sets sortingColumn and return builder. If the sortingColumn given in parameter is null, set sortingColumn to
         * empty string.
         * @param sortingColumn The sortingColumn value to set
         * @return The current builder
         */
        public Builder<T> sortingColumn(String sortingColumn) {
            if (search != null) {
                this.sortingColumn = sortingColumn;
            }
            return this;
        }

        /**
         * Sets sortingOrientation and return builder.
         * @param sortingOrientation The sortingOrientation value to set
         * @return The current builder
         */
        public Builder<T> sortingOrientation(SortingOrientation sortingOrientation) {
            this.sortingOrientation = sortingOrientation;
            return this;
        }

        /**
         * Sets sortingOrientation given in parameter if it matches with SortingOrientation enum value. If it does not
         * match set DEFAULT_SORTING_ORIENTATION
         * @param sortingOrientation The sorting orientation in string to convert
         * @return The current builder
         */
        public Builder<T> sortingOrientationFromStringOrDefault(String sortingOrientation) {
            if (sortingOrientation.equals(SortingOrientation.ASCENDING.getValue())) {
                this.sortingOrientation = SortingOrientation.ASCENDING;
            } else if (sortingOrientation.equals(SortingOrientation.DESCENDING.getValue())) {
                this.sortingOrientation = SortingOrientation.DESCENDING;
            } else {
                this.sortingOrientation = DEFAULT_SORTING_ORIENTATION;
            }
            return this;
        }

        /**
         * Build the SearchCriteria depending on previous settings.
         * @return The SearchCriteria
         */
        public SearchCriteria<T> build() {
            return new SearchCriteria<>(this);
        }
    }

    public enum SortingOrientation {
        ASCENDING(SORTING_ORIENTATION_ASCENDING),
        DESCENDING(SORTING_ORIENTATION_DESCENDING);

        /**
         * Create enum value with corresponding string symbol.
         * @param value The string symbol used for search
         */
        SortingOrientation(String value) {
            this.value = value;
        }
        private final String value;
        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    public Class<T> getTargetClass() {
        return targetClass;
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public int getPageSize() {
        return pageSize;
    }

    public String getSearch() {
        return "%" + search + "%";
    }

    public String getSortingColumn() {
        return sortingColumn;
    }

    /**
     * Returns the sortingColumn value or the given parameter if sortingColumn is empty.
     * @param defaultValue The defaultValue to return if sortingColumn is empty
     * @return Either sortingColumn if not empty either the default value
     */
    public String getSortingColumnOrDefault(String defaultValue) {
        return "".equals(sortingColumn) ? defaultValue : sortingColumn;
    }

    public SortingOrientation getSortingOrientation() {
        return sortingOrientation;
    }

    public int getStartPosition() {
        return pageIndex * pageSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SearchCriteria<?> that = (SearchCriteria<?>) o;
        return pageIndex == that.pageIndex &&
                pageSize == that.pageSize &&
                Objects.equals(targetClass, that.targetClass) &&
                Objects.equals(search, that.search) &&
                Objects.equals(sortingColumn, that.sortingColumn) &&
                sortingOrientation == that.sortingOrientation;
    }

    @Override
    public int hashCode() {
        return Objects.hash(targetClass, pageIndex, pageSize, search, sortingColumn, sortingOrientation);
    }

    @Override
    public String toString() {
        return "SearchCriteria{" +
                "targetClass=" + targetClass +
                ", pageIndex=" + pageIndex +
                ", pageSize=" + pageSize +
                ", search='" + search + '\'' +
                ", sortingColumn='" + sortingColumn + '\'' +
                ", sortingOrientation=" + sortingOrientation +
                '}';
    }
}
