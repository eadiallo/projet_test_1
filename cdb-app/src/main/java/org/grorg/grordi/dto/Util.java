package org.grorg.grordi.dto;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

final class Util {
    /**
     * Util is a static helper class and should not be instantiate.
     * @throws IllegalAccessException If we try to instantiate by reflexion
     */
    private Util() throws IllegalAccessException {
        throw new IllegalAccessException("Util is a static helper class and should not be instantiate");
    }

    /**
     * Transform a LocalDate to String using given format. If date given in parameter is null return null.
     * @param formatter The formatter to format date
     * @param date The date to transform
     * @return The date given in parameter to string if not null, null otherwise
     */
    static String fromLocalDateToStringDate(DateTimeFormatter formatter, LocalDate date) {
        return Optional.ofNullable(date).map(d -> d.format(formatter)).orElse(null);
    }

    /**
     * Transform a String date in ISO format to local date.
     * @param formatter The formatter to format date
     * @param date The string date in ISO format
     * @return The string date converted in LocalDate
     */
    static LocalDate fromStringDateToLocalDate(DateTimeFormatter formatter, String date) {
        return Optional.ofNullable(date).map(d -> LocalDate.parse(d, formatter)).orElse(null);
    }
}
