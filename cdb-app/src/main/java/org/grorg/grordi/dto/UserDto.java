package org.grorg.grordi.dto;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class UserDto {

    private List<String> roles = new ArrayList<>();

    public List<String> getRoles() {
        return new ArrayList<>(roles);
    }

    public void setRoles(List<String> roles) {
        this.roles = new ArrayList<>(roles);
    }
}
