package org.grorg.grordi.controller;

import org.grorg.grordi.dto.SearchCriteria;

public final class Paths {
    /**
     * Paths is a static helper class an must not be instantiate.
     * @throws IllegalAccessException When Paths is instantiate using reflexion
     */
    private Paths() throws IllegalAccessException {
        throw new IllegalAccessException("Paths class cannot be instantiate");
    }

    public static final String COMPUTERS = "/computers";
    public static final String COMPANIES = "/companies";
    public static final String LOGGEDIN = "/current/user";

    /**
     * Holds query parameters name.
     */
    public static final class QueryParam {
        /**
         * Default value is an helper static class that hold constants. It must not be instantiate.
         * @throws IllegalAccessException If we try to instantiate via reflexion
         */
        private QueryParam() throws IllegalAccessException {
            throw new IllegalAccessException("QueryParam is a static helper class and should not be instantiate");
        }

        public static final String PAGE_INDEX = "pageIndex";
        public static final String PAGE_SIZE = "pageSize";
        public static final String SEARCH = "search";
        public static final String COLUMN = "column";
        public static final String ORIENTATION  = "orientation";

        /**
         * Holds query parameters default value.
         */
        public static final class DefaultValue {
            /**
             * Default value is an helper static class that hold constants. It must not be instantiate.
             * @throws IllegalAccessException If we try to instantiate via reflexion
             */
            private DefaultValue() throws IllegalAccessException {
                throw new IllegalAccessException(
                        "DefaultValue is a static helper class and should not be instantiate");
            }
            public static final int PAGE_INDEX = SearchCriteria.DEFAULT_PAGE_INDEX;
            public static final int PAGE_SIZE = SearchCriteria.DEFAULT_PAGE_SIZE;
            public static final String SEARCH = SearchCriteria.DEFAULT_SEARCH;
            public static final String ORIENTATION  = SearchCriteria.DEFAULT_SORTING_ORIENTATION_STRING;

            /**
             * Holds all constants related to default value for computers.
             */
            public static final class Computers {
                /**
                 * Computers is a static helper class that hold constants. It must not be instantiate.
                 * @throws IllegalAccessException If we try to instantiate via reflexion
                 */
                private Computers() throws IllegalAccessException {
                    throw new IllegalAccessException(
                            "Computers is a static helper class and should not be instantiate");
                }

                public static final String COLUMN = "id";
            }

            /**
             * Holds all constants related to default value for companies.
             */
            public static final class Companies {
                /**
                 * Companies is a static helper class that hold constants. It must not be instantiate.
                 * @throws IllegalAccessException If we try to instantiate via reflexion
                 */
                private Companies() throws IllegalAccessException {
                    throw new IllegalAccessException(
                            "Companies is a static helper class and should not be instantiate");
                }

                public static final String COLUMN = "id";
            }
        }
    }
}
