package org.grorg.grordi.controller;

import org.grorg.grordi.dto.SearchCriteria;
import org.grorg.grordi.dto.error.Errors;
import org.grorg.grordi.entity.Company;
import org.grorg.grordi.service.CompanyService;
import org.grorg.grordi.service.ValidationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(Paths.COMPANIES)
public final class CompanyController {
    private static final Logger LOGGER = LoggerFactory.getLogger(CompanyController.class);
    private final CompanyService companyService;
    private final ValidationService validator;

    /**
     * Create a controller for url "/companies".
     * @param companyService Service used by the controller
     * @param validator Service used to control user input
     */
    public CompanyController(CompanyService companyService, ValidationService validator) {
        this.companyService = companyService;
        this.validator = validator;
    }

    /**
     * Provides Company as JSON object found in database with the id given in parameter.
      * @param id The company id to find
     * @return The found computer as JSON object or 404 error
     */
    @GetMapping("/{id}")
    public ResponseEntity findCompaniesById(@PathVariable("id") long id) {
        Company company = companyService.findCompanyById(id);
        if (company == null) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(Errors.COMPANY_NOT_FOUND);
        } else {
            return ResponseEntity.ok(company);
        }
    }

    /**
     * Search companies by multiple criteria given in parameter and return result as Page<Company>. The required
     * parameters are:
     * <ul>
     *     <li>pageIndex: The searched page index</li>
     *     <li>pageSize: The page size</li>
     *     <li>search: The search word(s) in company's name</li>
     *     <li>column: The base column to order the result</li>
     *     <li>orientation: The result orientation (either DESC either ASC)</li>
     * </ul>pageIndexThe searched page index
     * @param request The raw request to parse and must contains previous parameter
     * @return The found page result as Page<Company>
     */
    @GetMapping
    public ResponseEntity findCompaniesByCriteria(HttpServletRequest request) {
        SearchCriteria<Company> criteria = RequestParser.parseToCompanySearchCriteriaAndFillWithDefault(request);
        return validator
                .validateSearchCriteria(criteria)
                .fold(errors -> {
                            LOGGER.warn("Search criteria malformed {}", errors);
                            return ResponseEntity
                                    .status(HttpStatus.BAD_REQUEST)
                                    .body(errors);
                        },
                        validCriteria ->
                                ResponseEntity
                                        .ok(companyService.findCompaniesByCriteria(validCriteria)));
    }

    /**
     * Delete to company with the given id.
     * @param companyId The company id of the company to delete
     * @return A no content response
     */
    @DeleteMapping("/{companyId}")
    public ResponseEntity delete(@PathVariable long companyId) {
        companyService.deleteCompanyWithId(companyId);
        return ResponseEntity.noContent().build();
    }
}
