package org.grorg.grordi.controller;

import org.grorg.grordi.dto.ComputerWithCompanyIdDto;
import org.grorg.grordi.dto.ComputerWithCompanyIdDtoFactory;
import org.grorg.grordi.dto.ComputerWithCompanyNameDtoFactory;
import org.grorg.grordi.dto.SearchCriteria;
import org.grorg.grordi.dto.error.Error;
import org.grorg.grordi.dto.error.Errors;
import org.grorg.grordi.entity.Computer;
import org.grorg.grordi.service.ComputerService;
import org.grorg.grordi.service.ValidationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping(Paths.COMPUTERS)
public class ComputerController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ComputerController.class);
    private final ComputerService computerService;
    private final ValidationService validationService;

    /**
     * Create a controller for url "/computers".
     * @param computerService Service used by the controller
     * @param validationService Service used to control user input
     */
    public ComputerController(ComputerService computerService, ValidationService validationService) {
        this.computerService = computerService;
        this.validationService = validationService;
    }

    /**
     * Provide Computer found in database with id given in parameter as JSON object.
     * @param computerId The computer id to find
     * @return Computer found or 404 error
     */
    @GetMapping("/{computerId}")
    public ResponseEntity findById(@PathVariable long computerId) {
        Computer computer = computerService.findComputerById(computerId);
        if (computer != null) {
            return ResponseEntity.ok(ComputerWithCompanyIdDtoFactory.fromComputerToDto(computer));
        } else {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(Errors.COMPUTER_NOT_FOUND);
        }
    }

    /**
     * Search computer by multiple criteria given in parameter and return result as Page<ComputerWithCompanyNameDto>.
     * The query must contains those fields:
     * <ul>
     *   <li>pageIndex The searched page index</li>
     *   <li>pageSize The page size</li>
     *   <li>search The search word(s) in computer name or computer company name</li>
     *   <li>column The base column to order the result</li>
     *   <li>orientation The result orientation (either DESC either ASC)</li>
     * </ul>
     * @param request The request parameters
     * @return The found page result as Page<ComputerWithCompanyNameDto>
     */
    @GetMapping
    public ResponseEntity findByCriteria(HttpServletRequest request) {
        SearchCriteria<Computer> criteria = RequestParser.parseToComputerSearchCriteriaAndFillWithDefault(request);
        return validationService
                .validateSearchCriteria(criteria)
                .fold(errors -> {
                            LOGGER.warn("Search criteria malformed {}", errors);
                            return ResponseEntity
                                    .status(HttpStatus.BAD_REQUEST)
                                    .body(errors);
                        },
                        validCriteria ->
                            ResponseEntity
                                    .ok(computerService
                                            .findComputerByCriteria(validCriteria)
                                            .mapElements(ComputerWithCompanyNameDtoFactory::fromComputerToDto)));
    }

    /**
     * Create computer in database on post.
     * @param computer The computer to create
     * @return A 201 response with created computer as content or errors if malformed request body
     */
    @PostMapping
    public ResponseEntity create(@RequestBody ComputerWithCompanyIdDto computer) {
        return validationService
                .validateComputerWithCompanyIdDtoForCreation(computer)
                .fold(errors -> {
                            LOGGER.warn("Error during computer {} creation", computer);
                            return ResponseEntity
                                    .status(HttpStatus.BAD_REQUEST)
                                    .body(errors);
                        },
                        validComputer -> {
                            Computer createdComputer = computerService.createComputer(validComputer);
                            LOGGER.debug("Create new computer with id={}", createdComputer.getId());
                            return ResponseEntity
                                    .status(HttpStatus.CREATED)
                                    .body(ComputerWithCompanyIdDtoFactory
                                            .fromComputerToDto(createdComputer));
                        });
    }

    /**
     * Change error handling depending on list of error.
     * @param errors The error list found from computer checking for update
     * @param computer The dto checked, that is the source error
     * @return A response entity that contains the error list
     */
    private ResponseEntity<List<Error>> handleEditError(List<Error> errors, ComputerWithCompanyIdDto computer) {
        if (errors.contains(Errors.COMPUTER_NOT_FOUND)) {
            LOGGER.warn("Trying to update a computer that does not exists: {}", computer);
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(errors);
        } else {
            LOGGER.warn("Error during computer edition: {}", computer);
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(errors);
        }
    }

    /**
     * Edit the computer with the given id and body.
     * @param computerId The computer id to edit
     * @param computer The dto that contains the modification to persist
     * @return Either 200 when edition is performed, 404 if the computer to edit is not found, 400 if the computer
     * information is not valid
     */
    @PutMapping("/{computerId}")
    public ResponseEntity edit(@PathVariable long computerId, @RequestBody ComputerWithCompanyIdDto computer) {
        if (computer.getId() == null || computerId != computer.getId()) {
            LOGGER.warn("uri id={} does not correspond with request body={}", computerId, computer);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        return validationService.validateComputerWithCompanyIdDtoForUpdate(computer)
                .fold(errors -> handleEditError(errors, computer),
                        validComputer -> {
                            computerService.update(validComputer);
                            LOGGER.debug("Edit computer: {}", computer);
                            return ResponseEntity
                                    .status(HttpStatus.OK)
                                    .build();
                        });
    }

    /**
     * Delete the computer with the id given in parameter.
     * @param computerId The computer id to delete
     * @return A 204 (noContent) code
     */
    @DeleteMapping("/{computerId}")
    public ResponseEntity delete(@PathVariable long computerId) {
        computerService.deleteComputerWithId(computerId);
        return ResponseEntity.noContent().build();
    }
}
