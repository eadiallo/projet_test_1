package org.grorg.grordi.dao;

import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.dsl.PathBuilder;
import org.grorg.grordi.dto.SearchCriteria;

class Util {
    /**
     * Forbids the instantiation of static util class.
     */
    private Util() { }

    /**
     * Gets the order specifier depending on the sort column given by the criteria, or if the sort column is empty,
     * provides the order specifier for the given default field.
     * @param criteria The sort criteria
     * @param defaultField The default field to sort on if there is no field to sort specified in criteria
     * @param <T> The search criteria type
     * @return The ordering specifier computed from parameter
     */
    @SuppressWarnings("unchecked")
    // Uncheck assignment since we forge a specific order specifier from field as string, the type cannot be checked,
    // but this is not a problem
    static <T> OrderSpecifier getOrderSpecifierOrDefault(SearchCriteria<T> criteria, String defaultField) {
        final PathBuilder<T> builder = new PathBuilder<>(criteria.getTargetClass(),
                criteria.getTargetClass().getSimpleName().toLowerCase());
        final Order order = criteria.getSortingOrientation() == SearchCriteria.SortingOrientation.ASCENDING ?
                Order.ASC :
                Order.DESC;
        final String sortingColumn = criteria.getSortingColumn().isEmpty() ? defaultField : criteria.getSortingColumn();
        return new OrderSpecifier(order, builder.get(sortingColumn));
    }
}
