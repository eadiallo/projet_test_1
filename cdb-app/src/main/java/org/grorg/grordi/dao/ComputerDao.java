package org.grorg.grordi.dao;

import com.querydsl.jpa.impl.JPAQueryFactory;
import org.grorg.grordi.dto.SearchCriteria;
import org.grorg.grordi.entity.Computer;
import org.grorg.grordi.entity.QCompany;
import org.grorg.grordi.entity.QComputer;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;

@Repository
public class ComputerDao {
    private static final QComputer COMPUTER = QComputer.computer;

    @PersistenceContext
    private EntityManager entityManager;

    private JPAQueryFactory getQueryFactory() {
        return new JPAQueryFactory(entityManager);
    }

    /**
     * Persist the computer given in parameter and fill this parameter with creation information such as id.
     * @param computer The element to persist
     */
    public void create(Computer computer) {
        entityManager.persist(computer);
    }

    /**
     * Find the entity in database that have the same id than the one passed in parameter.
     * @param id The entity's id to find in database
     * @return The found entity instance or null if the entity does not exist
     */
    public Computer findById(long id) {
        return getQueryFactory()
                .selectFrom(COMPUTER)
                .where(COMPUTER.id.eq(id))
                .fetchOne();
    }

    /**
     * Returns the total number of found element for a search with given criteria.
     * @param criteria The search criteria
     * @return The number of found element
     */
    public long getTotalNumberOfFoundElementsByCriteria(SearchCriteria criteria) {
        return getQueryFactory()
                .select(COMPUTER)
                .from(COMPUTER)
                .leftJoin(COMPUTER.company, QCompany.company)
                .where(COMPUTER.name.like(criteria.getSearch())
                        .or(COMPUTER.company.name.like(criteria.getSearch())))
                .fetchCount();
    }

    /**
     * Find entities based on given criteria.
     * @param criteria The search criteria
     * @return The search result
     */
    @SuppressWarnings("unchecked")
    // suppress the unchecked assignment warning in the orderBy clause, because we build the predicate manually
    // and there is no type problem
    public Collection<Computer> findByCriteria(SearchCriteria criteria) {
        final String defaultSortField = "id";
        return getQueryFactory()
                .select(COMPUTER)
                .from(COMPUTER)
                .leftJoin(COMPUTER.company, QCompany.company)
                .where(COMPUTER.name.like(criteria.getSearch())
                        .or(COMPUTER.company.name.like(criteria.getSearch())))
                .limit(criteria.getPageSize())
                .offset(criteria.getStartPosition())
                .orderBy(Util.getOrderSpecifierOrDefault(criteria, defaultSortField))
                .fetch();
    }

    /**
     * Update the computer in database based on computer given in parameter.
     * @param computer The element to update
     */
    public void update(Computer computer) {
        entityManager.merge(computer);
    }

    /**
     * Delete the entity with the id given in parameter.
     * @param id The id of the entity to delete
     */
    public void delete(long id) {
        getQueryFactory()
                .delete(COMPUTER)
                .where(COMPUTER.id.eq(id))
                .execute();
    }

    /**
     * Tells if entity with id given in parameter exists.
     * @param id The entity id to check
     * @return true if the entity has been persisted, false otherwise
     */
    public boolean exists(long id) {
        return getQueryFactory()
                .select(COMPUTER.id)
                .from(COMPUTER)
                .where(COMPUTER.id.eq(id))
                .fetchCount() > 0;
    }
}
