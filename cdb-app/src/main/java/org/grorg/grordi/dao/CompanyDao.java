package org.grorg.grordi.dao;

import com.querydsl.jpa.impl.JPAQueryFactory;
import org.grorg.grordi.dto.SearchCriteria;
import org.grorg.grordi.entity.Company;
import org.grorg.grordi.entity.QCompany;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;

@Repository
public class CompanyDao {
    private static final QCompany COMPANY = QCompany.company;

    @PersistenceContext
    private EntityManager entityManager;

    private JPAQueryFactory getQueryFactory() {
        return new JPAQueryFactory(entityManager);
    }

    /**
     * Find the entity in database that have the same id than the one passed in parameter.
     * @param id The entity's id to find in database
     * @return The found entity instance or null if the entity does not exist
     */
    public Company findById(long id) {
        return getQueryFactory()
                .selectFrom(COMPANY)
                .where(COMPANY.id.eq(id))
                .fetchOne();
    }

    /**
     * Returns the total number of found element for a search with given criteria.
     * @param criteria The search criteria
     * @return The number of found element
     */
    public long getTotalNumberOfFoundElementsByCriteria(SearchCriteria criteria) {
        return getQueryFactory()
                .select(COMPANY.id)
                .from(COMPANY)
                .where(COMPANY.name.like(criteria.getSearch()))
                .fetchCount();
    }

    /**
     * Find entities based on given criteria.
     * @param criteria The search criteria
     * @return The search result
     */
    @SuppressWarnings("unchecked")
    // Suppress warning because we forge our proper research parameter and we loose the type information, but it's ok
    public Collection<Company> findByCriteria(SearchCriteria criteria) {
        final String defaultSortField = "id";
        return getQueryFactory()
                .selectFrom(COMPANY)
                .where(COMPANY.name.like(criteria.getSearch()))
                .limit(criteria.getPageSize())
                .offset(criteria.getStartPosition())
                .orderBy(Util.getOrderSpecifierOrDefault(criteria, defaultSortField))
                .fetch();
    }

    /**
     * Delete the entity with the id given in parameter.
     * @param id The id of the entity to delete
     */
    public void delete(long id) {
        getQueryFactory()
                .delete(COMPANY)
                .where(COMPANY.id.eq(id))
                .execute();
    }

    /**
     * Tells if entity with id given in parameter exists.
     * @param id The entity id to check
     * @return true if the entity has been persisted, false otherwise
     */
    public boolean exists(long id) {
        return getQueryFactory()
                .select(COMPANY.id)
                .from(COMPANY)
                .where(COMPANY.id.eq(id))
                .fetchCount() > 0;
    }
}
