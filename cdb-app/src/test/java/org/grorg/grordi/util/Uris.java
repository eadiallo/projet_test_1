package org.grorg.grordi.util;

import org.grorg.grordi.controller.Paths;

public enum  Uris {
    COMPANY(Paths.COMPANIES),
    COMPUTER(Paths.COMPUTERS);

    private final String path;

    Uris(String path) {
        this.path = path;
    }

    private String getCompany(Long id) {
        return path + (id != null ? "/" + id : "");
    }

    public String get() {
        return getCompany(null);
    }

    public String get(long id) {
        return getCompany(id);
    }
}
