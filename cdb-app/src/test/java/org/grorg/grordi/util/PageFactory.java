package org.grorg.grordi.util;

import org.grorg.grordi.controller.Paths;
import org.grorg.grordi.dto.ComputerWithCompanyNameDto;
import org.grorg.grordi.dto.ComputerWithCompanyNameDtoFactory;
import org.grorg.grordi.dto.Page;
import org.grorg.grordi.entity.Company;

import java.util.Collection;
import java.util.stream.Collectors;

public class PageFactory {
    private static <T> Page.Builder<T> getDefaultBuilder(Collection<T> elements) {
        return Page.<T>builder()
                .pageIndex(Paths.QueryParam.DefaultValue.PAGE_INDEX)
                .pageSize(Paths.QueryParam.DefaultValue.PAGE_SIZE)
                .search(Paths.QueryParam.DefaultValue.SEARCH)
                .column(Paths.QueryParam.DefaultValue.Companies.COLUMN)
                .orientation(Paths.QueryParam.DefaultValue.ORIENTATION)
                .totalPage(1)
                .totalElements(Consts.Computer.TOTAL_NUMBER_IN_DATABASE)
                .elements(elements);
    }

    public static Page<ComputerWithCompanyNameDto> getAllComputer() {
        return getDefaultBuilder(ComputerFactory.allComputerInDatabase()
                .stream()
                .map(ComputerWithCompanyNameDtoFactory::fromComputerToDto).collect(Collectors.toList()))
                .build();
    }

    public static Page<ComputerWithCompanyNameDto> getAllDescendingComputer() {
        return getDefaultBuilder(ComputerFactory.allCompanyInDatabaseReverse()
                .stream()
                .map(ComputerWithCompanyNameDtoFactory::fromComputerToDto).collect(Collectors.toList()))
                .orientation(Consts.SearchCriteria.Valid.Orientation.DESCENDING)
                .build();
    }

    public static Page<Company> getAllCompany() {
        return getDefaultBuilder(CompanyFactory.allCompanyInDatabase())
                .totalElements(Consts.Company.TOTAL_NUMBER_IN_DATABASE)
                .build();
    }
}
