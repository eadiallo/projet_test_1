package org.grorg.grordi.util;

import org.grorg.grordi.entity.Operation;

public class OperationFactory {
    public static Operation newValidOperationWithoutId() {
        return Operation.builder().forComputer().withEntityId(1).ofInsertionType().build();
    }
}
