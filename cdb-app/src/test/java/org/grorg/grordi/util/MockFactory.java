package org.grorg.grordi.util;

import org.grorg.grordi.dao.ComputerDao;
import org.grorg.grordi.entity.Computer;
import org.grorg.grordi.service.CompanyService;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

public class MockFactory {
    public static void mockComputerDaoCreation(ComputerDao computerDao,
                                               Computer nonExistingComputer, long newComputerId) {
        final int firstArgument = 0;
        doAnswer(methodArgs -> {
            Computer computerArg = methodArgs.getArgument(firstArgument);
            computerArg.setId(newComputerId);
            return null;
        }).when(computerDao)
                .create(nonExistingComputer);
    }

    public static void mockCompanyServiceFindCompanyById(CompanyService companyService) {
        final long validCompanyId = Consts.Computer.Valid.ID;
        when(companyService.findCompanyById(eq(validCompanyId)))
                .thenReturn(CompanyFactory.newCompanyWithId(validCompanyId));
    }
}
