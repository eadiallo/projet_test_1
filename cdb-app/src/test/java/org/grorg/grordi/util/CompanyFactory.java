package org.grorg.grordi.util;

import org.grorg.grordi.entity.Company;

import java.util.Arrays;
import java.util.List;

public class CompanyFactory {
    public static final long FIRST_COMPANY_ID_DATABASE = 1;
    public static Company newFirstCompanyInDatabase() {
        return newAppleCompany();
    }

    public static Company newAppleCompany() {
        return new Company(Consts.Company.Apple.ID, Consts.Company.Apple.NAME);
    }

    public static Company newThinkingMachines() {
        return new Company(Consts.Company.ThinkingMachines.ID, Consts.Company.ThinkingMachines.Name);
    }

    public static Company newValidCompany() {
        return new Company(Consts.Company.Valid.ID, Consts.Company.Valid.NAME);
    }

    public static Company newCompanyWithId(long id) {
        return new Company(id, Consts.Company.Valid.NAME);
    }

    public static List<Company> allCompanyInDatabase() {
        return Arrays.asList(newAppleCompany(), newThinkingMachines());
    }

    public static List<Company> allCompanyInDatabaseSortByNameReverse() {
        return Arrays.asList(newThinkingMachines(), newAppleCompany());
    }
}
