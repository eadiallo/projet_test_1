package org.grorg.grordi.controller;

import org.grorg.grordi.dto.SearchCriteria;
import org.grorg.grordi.entity.Company;
import org.grorg.grordi.entity.Computer;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class RequestParserTest {
    @Test
    void parseCompanySearchCriteriaShouldReturnADefaultSearchCriteriaIfDefaultRequest() {
        assertEquals(SearchCriteria.builder(Company.class)
                        .sortingColumn(Paths.QueryParam.DefaultValue.Companies.COLUMN)
                        .build(),
                RequestParser.parseToCompanySearchCriteriaAndFillWithDefault(new MockHttpServletRequest()));
    }

    @Test
    void parseCompanySearchCriteriaShouldThrowIllegalArgumentExceptionOnNonNumberFieldParsing() {
        MockHttpServletRequest servletRequest = new MockHttpServletRequest();
        servletRequest.addParameter(Paths.QueryParam.PAGE_SIZE, "invalid");
        assertThrows(IllegalArgumentException.class,
                () -> RequestParser.parseToCompanySearchCriteriaAndFillWithDefault(servletRequest));
    }

    @Test
    void parseCompanySearchCriteriaShouldReturnCorrectObjectForCustomParameter() {
        final int pageSize = 12;
        final int pageIndex = 12;
        final String search = "search";
        final String column = "name";
        final String orientation = "DESC";
        MockHttpServletRequest servletRequest = new MockHttpServletRequest();
        servletRequest.addParameter(Paths.QueryParam.PAGE_SIZE, String.valueOf(pageSize));
        servletRequest.addParameter(Paths.QueryParam.PAGE_INDEX, String.valueOf(pageIndex));
        servletRequest.addParameter(Paths.QueryParam.SEARCH, search);
        servletRequest.addParameter(Paths.QueryParam.COLUMN, column);
        servletRequest.addParameter(Paths.QueryParam.ORIENTATION, orientation);

        assertEquals(SearchCriteria.builder(Company.class)
                .pageSize(pageSize)
                .pageIndex(pageIndex)
                .search(search)
                .sortingColumn(column)
                .sortingOrientationFromStringOrDefault(orientation)
                .build(),
                RequestParser.parseToCompanySearchCriteriaAndFillWithDefault(servletRequest));
    }

    @Test
    void parseComputerSearchCriteriaShouldReturnADefaultSearchCriteriaIfDefaultRequest() {
        assertEquals(SearchCriteria.builder(Computer.class)
                        .sortingColumn(Paths.QueryParam.DefaultValue.Companies.COLUMN)
                        .build(),
                RequestParser.parseToComputerSearchCriteriaAndFillWithDefault(new MockHttpServletRequest()));
    }

    @Test
    void parseComputerSearchCriteriaShouldThrowIllegalArgumentExceptionOnNonNumberFieldParsing() {
        MockHttpServletRequest servletRequest = new MockHttpServletRequest();
        servletRequest.addParameter(Paths.QueryParam.PAGE_SIZE, "invalid");
        assertThrows(IllegalArgumentException.class,
                () -> RequestParser.parseToComputerSearchCriteriaAndFillWithDefault(servletRequest));
    }

    @Test
    void parseComputerSearchCriteriaShouldReturnCorrectObjectForCustomParameter() {
        final int pageSize = 12;
        final int pageIndex = 12;
        final String search = "search";
        final String column = "name";
        final String orientation = "DESC";
        MockHttpServletRequest servletRequest = new MockHttpServletRequest();
        servletRequest.addParameter(Paths.QueryParam.PAGE_SIZE, String.valueOf(pageSize));
        servletRequest.addParameter(Paths.QueryParam.PAGE_INDEX, String.valueOf(pageIndex));
        servletRequest.addParameter(Paths.QueryParam.SEARCH, search);
        servletRequest.addParameter(Paths.QueryParam.COLUMN, column);
        servletRequest.addParameter(Paths.QueryParam.ORIENTATION, orientation);

        assertEquals(SearchCriteria.builder(Computer.class)
                        .pageSize(pageSize)
                        .pageIndex(pageIndex)
                        .search(search)
                        .sortingColumn(column)
                        .sortingOrientationFromStringOrDefault(orientation)
                        .build(),
                RequestParser.parseToComputerSearchCriteriaAndFillWithDefault(servletRequest));
    }
}
