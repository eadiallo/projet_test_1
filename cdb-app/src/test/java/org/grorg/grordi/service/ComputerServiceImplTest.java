package org.grorg.grordi.service;

import org.grorg.grordi.dao.ComputerDao;
import org.grorg.grordi.dao.OperationDao;
import org.grorg.grordi.entity.Computer;
import org.grorg.grordi.util.ComputerFactory;
import org.grorg.grordi.util.MockFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ComputerServiceImplTest {
    @Mock
    private ComputerDao computerDao;
    @Mock
    private OperationDao operationDao;
    @InjectMocks
    private ComputerService testedService;

    @Test
    void createdComputerShouldBeReturnedWithItsId() {
        Computer computer = new Computer();
        final long newComputerId = 1;
        MockFactory.mockComputerDaoCreation(computerDao, computer, newComputerId);
        computer.setName("computer");
        Computer createdComputer = testedService.createComputer(new ValidForCreation<>(computer));
        assertSame(newComputerId, computer.getId());
        assertSame(createdComputer, computer);
        verify(computerDao).create(computer);
        verify(operationDao).create(any());
    }

    @Test
    void existingComputerShouldBeReturned() {
        final long existingId = 1;
        when(computerDao.findById(existingId)).thenReturn(ComputerFactory.newValidComputer());
        assertEquals(ComputerFactory.newValidComputer(), testedService.findComputerById(existingId));
        verify(computerDao).findById(existingId);
    }

    @Test
    void nonExistingComputerShouldReturnNull() {
        final long nonExistingId = 0;
        when(computerDao.findById(nonExistingId)).thenReturn(null);
        assertNull(testedService.findComputerById(nonExistingId));
        verify(computerDao).findById(nonExistingId);
    }

    @Test
    void updateExistingComputerShouldUpdateItIfItExists() {
        Computer validComputer = ComputerFactory.newValidComputer();
        validComputer.setName("UPDATE");
        when(computerDao.exists(validComputer.getId())).thenReturn(true);
        testedService.update(new ValidForUpdate<>(validComputer));
        verify(computerDao).exists(validComputer.getId());
        verify(computerDao).update(validComputer);
        verify(operationDao).create(any());
    }

    @Test
    void updateNonExistingComputerShouldReturnFalse() {
        Computer validComputer = ComputerFactory.newValidComputer();
        when(computerDao.exists(validComputer.getId())).thenReturn(false);
        testedService.update(new ValidForUpdate<>(validComputer));
        verify(computerDao).exists(validComputer.getId());
        verify(computerDao, never()).create(validComputer);
        verify(operationDao, never()).create(any());
    }

    @Test
    void deleteComputeShouldDeleteIt() {
        final long computerId = 1;
        testedService.deleteComputerWithId(computerId);
        verify(computerDao).delete(computerId);
        verify(operationDao).create(any());
    }
}
