package org.grorg.grordi.service;

import org.grorg.grordi.dto.ComputerWithCompanyIdDto;
import org.grorg.grordi.dto.ComputerWithCompanyIdDtoFactory;
import org.grorg.grordi.dto.SearchCriteria;
import org.grorg.grordi.dto.error.Errors;
import org.grorg.grordi.entity.Computer;
import org.grorg.grordi.util.ComputerDtoFactory;
import org.grorg.grordi.util.ComputerFactory;
import org.grorg.grordi.util.Consts;
import io.vavr.control.Either;
import org.grorg.grordi.util.MockFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ValidatorServiceTest {
    @Mock
    private ComputerService computerService;
    @Mock
    private CompanyService companyService;
    @InjectMocks
    private ValidationService testedService;

    @Test
    void validComputerShouldBeValidateForCreation() {
        MockFactory.mockCompanyServiceFindCompanyById(companyService);
        ComputerWithCompanyIdDto computerWithoutId = ComputerDtoFactory.newComputerWithIntroducedDateWithoutId();
        assertEquals(Either.right(new ValidForCreation<>(ComputerFactory.newComputerWithIntroducedDateWithoutId())),
                testedService.validateComputerWithCompanyIdDtoForCreation(computerWithoutId));
        verify(companyService).findCompanyById(computerWithoutId.getCompany());
    }

    @Test
    void validComputerWithAnIdShouldReturnErrorOnValidationForCreation() {
        MockFactory.mockCompanyServiceFindCompanyById(companyService);
        ComputerWithCompanyIdDto validComputer = ComputerDtoFactory.newValidComputer();
        assertEquals(Either.left(Collections.singletonList(Errors.COMPUTER_TO_CREATE_SHOULD_NOT_HAVE_ID)),
                testedService.validateComputerWithCompanyIdDtoForCreation(validComputer));
        verify(companyService).findCompanyById(validComputer.getCompany());
    }

    @Test
    void nullComputerShouldReturnErrorOnValidationForCreation() {
        assertEquals(Either.left(Collections.singletonList(Errors.COMPUTER_SHOULD_NOT_BE_EMPTY)),
                testedService.validateComputerWithCompanyIdDtoForCreation(null));
    }

    @Test
    void computerWithInvalidCompanyShouldReturnErrorOnValidationForCreation() {
        ComputerWithCompanyIdDto computerWithInvalidCompany =
                ComputerDtoFactory.newComputerWithoutIdWithInvalidCompany();
        assertEquals(Either.left(Collections.singletonList(Errors.COMPUTER_SHOULD_HAVE_VALID_COMPANY)),
                testedService.validateComputerWithCompanyIdDtoForCreation(computerWithInvalidCompany));
        verify(companyService).findCompanyById(computerWithInvalidCompany.getCompany());
    }

    @Test
    void validSearchCriteriaShouldBeValidate() {
        SearchCriteria<Computer> criteria = SearchCriteria.defaultCriteria(Computer.class);
        assertEquals(Either.right(new Valid<>(SearchCriteria.defaultCriteria(Computer.class))),
                testedService.validateSearchCriteria(criteria));
    }

    @Test
    void searchCriteriaWithInvalidSortingColumnShouldReturnsError() {
        SearchCriteria<Computer> invalidCriteria = SearchCriteria
                .builder(Computer.class)
                .sortingColumn(Consts.SearchCriteria.Computer.Invalid.COLUMN)
                .build();
        assertEquals(Either.left(Collections.singletonList(
                Errors.SEARCH_CRITERIA_SORTING_COLUMN_SHOULD_MATCH_WITH_COMPUTER_FIELDS)),
                testedService.validateSearchCriteria(invalidCriteria));
    }

    @Test
    void searchCriteriaWithInvalidSortingColumnForCompanyShouldReturnsError() {
        SearchCriteria<Computer> invalidCriteria = SearchCriteria
                .builder(Computer.class)
                .sortingColumn(Consts.SearchCriteria.Company.Invalid.COLUMN)
                .build();
        assertEquals(Either.left(Collections.singletonList(
                Errors.SEARCH_CRITERIA_SORTING_COLUMN_SHOULD_MATCH_WITH_COMPUTER_FIELDS)),
                testedService.validateSearchCriteria(invalidCriteria));
    }

    @Test
    void searchCriteriaWithToHighPageSizeShouldReturnsErrorOnValidation() {
        SearchCriteria<Computer> invalidCriteria = SearchCriteria
                .builder(Computer.class)
                .pageSize(Consts.SearchCriteria.Invalid.PAGE_SIZE)
                .build();
        assertEquals(Either.left(Collections.singletonList(
                Errors.SEARCH_CRITERIA_PAGE_SIZE_SHOULD_NOT_BE_HIGHER_THAN_100)),
                testedService.validateSearchCriteria(invalidCriteria));
    }

    @Test
    void searchCriteriaWithToLowPageSizeShouldReturnsErrorOnValidation() {
        SearchCriteria<Computer> invalidCriteria = SearchCriteria
                .builder(Computer.class)
                .pageSize(Consts.SearchCriteria.Invalid.PAGE_SIZE_TO_LOW)
                .build();
        assertEquals(Either.left(Collections.singletonList(
                Errors.SEARCH_CRITERIA_PAGE_SIZE_SHOULD_BE_HIGHER_THAN_0)),
                testedService.validateSearchCriteria(invalidCriteria));
    }

    @Test
    void searchCriteriaWithToLowPageIndexShouldReturnsErrorOnValidation() {
        SearchCriteria<Computer> invalidCriteria = SearchCriteria
                .builder(Computer.class)
                .pageIndex(Consts.SearchCriteria.Invalid.PAGE_INDEX)
                .build();
        assertEquals(Either.left(Collections.singletonList(
                Errors.SEARCH_CRITERIA_PAGE_INDEX_SHOULD_BE_HIGHER_OR_EQUAL_TO_0)),
                testedService.validateSearchCriteria(invalidCriteria));
    }

    @Test
    void invalidSearchCriteriaShouldReturnsAllErrorsOnValidation() {
        SearchCriteria<Computer> invalidCriteria = SearchCriteria
                .builder(Computer.class)
                .sortingColumn(Consts.SearchCriteria.Computer.Invalid.COLUMN)
                .pageSize(Consts.SearchCriteria.Invalid.PAGE_SIZE)
                .build();
        assertEquals(Either.left(Arrays.asList(Errors.SEARCH_CRITERIA_SORTING_COLUMN_SHOULD_MATCH_WITH_COMPUTER_FIELDS,
                Errors.SEARCH_CRITERIA_PAGE_SIZE_SHOULD_NOT_BE_HIGHER_THAN_100)),
                testedService.validateSearchCriteria(invalidCriteria));
    }

    @Test
    void nonExistingComputerIsNotValidForUpdate() {
        ComputerWithCompanyIdDto nonExistingComputer = ComputerDtoFactory.newNonExistingComputer();
        assertEquals(Either.left(Collections.singletonList(Errors.COMPUTER_NOT_FOUND)),
                testedService.validateComputerWithCompanyIdDtoForUpdate(nonExistingComputer));
        verify(computerService).findComputerById(nonExistingComputer.getId());
    }

    @Test
    void updateComputerCompanyFieldShouldReturnError() {
        final Computer validBase = ComputerFactory.newValidComputer();
        final ComputerWithCompanyIdDto computerDto = ComputerWithCompanyIdDtoFactory.fromComputerToDto(validBase);
        computerDto.setCompany(Consts.Company.Invalid.ID);
        when(computerService.findComputerById(validBase.getId())).thenReturn(validBase);

        assertEquals(Either.left(Collections.singletonList(Errors.COMPUTER_COMPANY_FIELD_CANNOT_BE_UPDATE)),
                testedService.validateComputerWithCompanyIdDtoForUpdate(computerDto));
    }

    @Test
    void updateComputerCompanyFieldFromNullToSomethingShouldReturnError() {
        final Computer validBase = ComputerFactory.newValidComputer();
        final ComputerWithCompanyIdDto computerDto = ComputerWithCompanyIdDtoFactory.fromComputerToDto(validBase);
        validBase.setCompany(null);
        when(computerService.findComputerById(validBase.getId())).thenReturn(validBase);

        assertEquals(Either.left(Collections.singletonList(Errors.COMPUTER_COMPANY_FIELD_CANNOT_BE_UPDATE)),
                testedService.validateComputerWithCompanyIdDtoForUpdate(computerDto));
    }

    @Test
    void updateValidComputerShouldValidateComputer() {
        final Computer validBase = ComputerFactory.newValidComputer();
        final ComputerWithCompanyIdDto computerDto = ComputerWithCompanyIdDtoFactory.fromComputerToDto(validBase);
        when(computerService.findComputerById(validBase.getId())).thenReturn(validBase);

        assertEquals(Either.right(new ValidForUpdate<>(validBase)),
                testedService.validateComputerWithCompanyIdDtoForUpdate(computerDto));
    }
}
