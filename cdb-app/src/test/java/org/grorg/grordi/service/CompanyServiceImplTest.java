package org.grorg.grordi.service;

import org.grorg.grordi.dao.CompanyDao;
import org.grorg.grordi.dao.OperationDao;
import org.grorg.grordi.dto.SearchCriteria;
import org.grorg.grordi.entity.Company;
import org.grorg.grordi.util.CompanyFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CompanyServiceImplTest {
    @Mock
    private CompanyDao companyDao;
    @Mock
    private OperationDao operationDao;
    @InjectMocks
    private CompanyService testedService;

    @Test
    void findAnExistingCompanyShouldReturnIt() {
        final long existingId = 1;
        when(companyDao.findById(existingId)).thenReturn(CompanyFactory.newCompanyWithId(existingId));
        assertEquals(CompanyFactory.newCompanyWithId(existingId), testedService.findCompanyById(existingId));
        verify(companyDao).findById(existingId);
    }

    @Test
    void findAnNonExistingCompanyShouldReturnNull() {
        final long existingId = 0;
        assertNull(testedService.findCompanyById(existingId));
        verify(companyDao).findById(existingId);
    }

    @Test
    void findByCriteriaShouldBeUsed() {
        Valid<SearchCriteria<Company>> searchCriteria = new Valid<>(SearchCriteria.defaultCriteria(Company.class));
        testedService.findCompaniesByCriteria(searchCriteria);
        verify(companyDao).findByCriteria(searchCriteria.get());
    }

    @Test
    void deleteCompanyShouldCallDeleteDaoAndLogOperation() {
        final long computerIdToDelete = 1;
        testedService.deleteCompanyWithId(computerIdToDelete);
        verify(companyDao).delete(computerIdToDelete);
        verify(operationDao).create(any());
    }
}
