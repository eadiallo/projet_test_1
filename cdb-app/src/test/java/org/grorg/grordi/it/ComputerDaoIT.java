package org.grorg.grordi.it;

import org.grorg.grordi.config.PersistenceConfig;
import org.grorg.grordi.dao.ComputerDao;
import org.grorg.grordi.dto.SearchCriteria;
import org.grorg.grordi.entity.Computer;
import org.grorg.grordi.util.ComputerFactory;
import org.grorg.grordi.util.Consts;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = PersistenceConfig.class)
@DataJpaTest
@Sql("classpath:insert.sql")
class ComputerDaoIT {
    private static final String APPLE_COMPUTER_SEARCH = "Apple";
    private final ComputerDao computerDao;

    @Autowired
    ComputerDaoIT(ComputerDao computerDao) {
        this.computerDao = computerDao;
    }

    @Test
    void createComputerShouldAddItInDatabase() {
        Computer computer = ComputerFactory.newValidAppleComputerWithoutId();
        computerDao.create(computer);

        assertNotNull(computer.getId());
        final long newComputerId = computer.getId();

        Computer validComputer = ComputerFactory.newValidAppleComputerWithoutId();
        validComputer.setId(newComputerId);
        assertEquals(validComputer, computer);

        Computer createdComputer = computerDao.findById(newComputerId);
        assertEquals(computer, createdComputer);
    }

    @Test
    void createComputerWithDateShouldAddItInDatabase() {
        Computer computer = ComputerFactory.newValidAppleComputerWithoutIdWithDates();
        computerDao.create(computer);

        assertNotNull(computer.getId());
        final long newComputerId = computer.getId();

        Computer computerWithId = ComputerFactory.newValidAppleComputerWithoutIdWithDates();
        computerWithId.setId(newComputerId);

        Computer createdComputer = computerDao.findById(newComputerId);
        assertEquals(computer, createdComputer);
    }

    @Test
    void getComputerByIdShouldBeReturnedIfComputerExistsInDB() {
        final long firstComputerId = Consts.Computer.FIRST_ID_IN_DATABASE;
        Computer firstComputerInDatabase = computerDao.findById(firstComputerId);
        Computer firstComputer = ComputerFactory.newFirstComputerInDatabase();
        assertEquals(firstComputer, firstComputerInDatabase);
    }

    @Test
    void getComputerByIdShouldReturnNullIfComputerDoesNotExistInDB() {
        final long computerIdThatDoesNotExist = 0;
        Computer computerThatDoesNotExist = computerDao.findById(computerIdThatDoesNotExist);
        assertNull(computerThatDoesNotExist);
    }

    @Test
    void updateComputerShouldUpdateEntityInDatabase() {
        Computer computer = ComputerFactory.newFirstComputerInDatabase();
        computer.setName("UPDATE");
        computer.setIntroduced(LocalDate.of(1972, 2, 1));
        computer.setDiscontinued(LocalDate.of(1984, 2, 1));
        computerDao.update(computer);
        Computer updatedComputer = computerDao.findById(computer.getId());
        assertEquals(computer, updatedComputer);
    }

    @Test
    void deleteComputerShouldRemoveItInDatabase() {
        final long firstComputerIdInDatabase = Consts.Computer.FIRST_ID_IN_DATABASE;
        computerDao.delete(firstComputerIdInDatabase);
        Computer deletedComputer = computerDao.findById(firstComputerIdInDatabase);
        assertNull(deletedComputer);
    }

    @Test
    void existingComputerInDatabaseShouldReturnTrueOnExistsTest() {
        final long firstComputerIdInDatabase = Consts.Computer.FIRST_ID_IN_DATABASE;
        assertTrue(computerDao.exists(firstComputerIdInDatabase));
    }

    @Test
    void nonExistingComputerInDatabaseShouldReturnFalseOnExistsTest() {
        final long computerIdThatDoesNotExist = 0;
        assertFalse(computerDao.exists(computerIdThatDoesNotExist));
    }

    @Test
    void lookingForAllComputerShouldReturnFirstComputerPage() {
        Collection<Computer> computers = computerDao.findByCriteria(SearchCriteria.defaultCriteria(Computer.class));
        assertEquals(ComputerFactory.allComputerInDatabase(), computers);
    }

    @Test
    void lookingForAllComputerWithPageSizeOf1ShouldReturnOnePage() {
        Collection<Computer> computers = computerDao.findByCriteria(SearchCriteria
                .builder(Computer.class)
                .pageSize(1)
                .build());
        assertEquals(Collections.singletonList(ComputerFactory.newFirstComputerInDatabase()),
                computers);
    }

    @Test
    void lookingForAllComputerLimitTo5ShouldReturnFiveFirstComputerPage() {
        Collection<Computer> computers = computerDao.findByCriteria(SearchCriteria.builder(Computer.class).pageSize(5)
                .build());
        assertEquals(ComputerFactory
                        .allComputerInDatabase()
                        .stream()
                        .limit(5)
                        .collect(Collectors.toList()), computers);
    }

    @Test
    void lookingForSecondComputerPageLimitTo5ShouldReturnFiveSecondComputers() {
        Collection<Computer> computers = computerDao.findByCriteria(
                SearchCriteria.builder(Computer.class).pageIndex(1).pageSize(5).build());
        assertEquals(ComputerFactory
                        .allComputerInDatabase()
                        .stream()
                        .skip(5)
                        .limit(5)
                        .collect(Collectors.toList()), computers);
    }

    @Test
    void lookingForAllComputerWithAppleInNameReturnOnlyComputerWithAppleInName() {
        Collection<Computer> computers = computerDao.findByCriteria(
                SearchCriteria.builder(Computer.class).search(APPLE_COMPUTER_SEARCH).build());
        assertEquals(ComputerFactory.allAppleComputer(), computers);
    }

    @Test
    void lookingForNonExistingComputerReturnsEmptyResult() {
        Collection<Computer> computers = computerDao.findByCriteria(
                SearchCriteria.builder(Computer.class).search("EMPTY").build());
        assertEquals(Collections.emptyList(), computers);
    }

    @Test
    void lookingForAllAppleComputerSortByNameShouldReturnAllAppleComputerSortByName() {
        final String sortingColumn = "name";
        assertEquals(ComputerFactory.allAppleComputerSortedByName(), computerDao.findByCriteria(SearchCriteria
                .builder(Computer.class)
                .search(APPLE_COMPUTER_SEARCH)
                .sortingColumn(sortingColumn)
                .build()));
    }

    @Test
    void getTotalNumberOfFoundElementShouldReturnTheNumberOfFoundElement() {
        long numberOfFoundElement = computerDao.getTotalNumberOfFoundElementsByCriteria(SearchCriteria
                .builder(Computer.class)
                .search("Apple")
                .build());
        final long expectedNumberOfFoundElement = ComputerFactory.allAppleComputer().size();
        assertEquals(expectedNumberOfFoundElement, numberOfFoundElement);
    }

    @Test
    void getTotalNumberOfFoundElementShouldReturnZeroForNotFoundResearch() {
        long numberOfFoundElement = computerDao.getTotalNumberOfFoundElementsByCriteria(SearchCriteria
                .builder(Computer.class)
                .search("EMPTY")
                .build());
        final long expectedNumberOfFoundElement = 0;
        assertEquals(expectedNumberOfFoundElement, numberOfFoundElement);
    }
}
