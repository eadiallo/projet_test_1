package org.grorg.grordi.it;

import org.grorg.grordi.config.PersistenceConfig;
import org.grorg.grordi.dao.CompanyDao;
import org.grorg.grordi.dao.ComputerDao;
import org.grorg.grordi.dto.SearchCriteria;
import org.grorg.grordi.entity.Company;
import org.grorg.grordi.entity.Computer;
import org.grorg.grordi.util.CompanyFactory;
import org.grorg.grordi.util.Consts;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collection;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = PersistenceConfig.class)
@DataJpaTest
@Sql("classpath:insert.sql")
class CompanyDaoIT {
    private final CompanyDao companyDao;
    private final ComputerDao computerDao;

    @Autowired
    CompanyDaoIT(CompanyDao companyDao, ComputerDao computerDao) {
        this.companyDao = companyDao;
        this.computerDao = computerDao;
    }

    @Test
    void getCompanyByIdShouldReturnItIfCompanyExistsInDatabase() {
        final long firstCompanyId = 1;
        Company company = companyDao.findById(firstCompanyId);
        Company firstCompany = CompanyFactory.newFirstCompanyInDatabase();
        assertEquals(firstCompany, company);
    }

    @Test
    void getCompanyByIdShouldReturnNullIfCompanyDoesNotExistInDatabase() {
        final long companyIdThatDoesNotExist = 0;
        Company company = companyDao.findById(companyIdThatDoesNotExist);
        assertNull(company);
    }

    @Test
    void deleteCompanyShouldDeleteItAndAllTheAssociateComputer() {
        final long deleteId = Consts.Company.Apple.ID;
        companyDao.delete(deleteId);

        // verify the company does not exist anymore
        assertNull(companyDao.findById(deleteId));

        // verify the computer associate to the company are deleted
        assertEquals(Collections.emptyList(),
                computerDao.findByCriteria(SearchCriteria.builder(Computer.class)
                        .search(Consts.Company.Apple.NAME)
                        .build()));
    }

    @Test
    void existingComputerInDatabaseShouldReturnTrueOnExistsTest() {
        assertTrue(companyDao.exists(CompanyFactory.FIRST_COMPANY_ID_DATABASE));
    }

    @Test
    void nonExistingComputerInDatabaseShouldReturnFalseOnExistsTest() {
        final long companyIdThatDoesNotExist = 0;
        assertFalse(companyDao.exists(companyIdThatDoesNotExist));
    }

    @Test
    void lookingForAllCompanyShouldReturnFirstCompanyPage() {
        Collection<Company> companies = companyDao.findByCriteria(SearchCriteria.defaultCriteria(Company.class));
        assertEquals(CompanyFactory.allCompanyInDatabase(), companies);
    }

    @Test
    void lookingForAllCompanyWithPageSizeOf1ShouldReturnOnePage() {
        Collection<Company> companies = companyDao.findByCriteria(SearchCriteria
                .builder(Company.class)
                .pageSize(1)
                .build());
        assertEquals(Collections.singletonList(CompanyFactory.newAppleCompany()), companies);
    }

    @Test
    void lookingForSecondPageOfAllCompanyWithPageSizeOf1ShouldReturnOnePage() {
        Collection<Company> companies = companyDao.findByCriteria(SearchCriteria
                .builder(Company.class)
                .pageSize(1)
                .pageIndex(1)
                .build());
        assertEquals(Collections.singletonList(CompanyFactory.newThinkingMachines()), companies);
    }

    @Test
    void lookingForAppleCompanyShouldReturnAppleCompany() {
        Collection<Company> companies = companyDao.findByCriteria(SearchCriteria
                .builder(Company.class)
                .search(Consts.Company.Apple.NAME)
                .build());
        assertEquals(Collections.singletonList(CompanyFactory.newAppleCompany()), companies);
    }

    @Test
    void lookingForNonExistingCompanyReturnsEmptyResult() {
        Collection<Company> companies = companyDao.findByCriteria(
                SearchCriteria.builder(Company.class).search("EMPTY").build());
        assertEquals(Collections.emptyList(), companies);
    }

    @Test
    void lookingForAllCompanySortByNameReverseOrderShouldReturnCorrectResult() {
        final String sortingColumn = "name";
        assertEquals(CompanyFactory.allCompanyInDatabaseSortByNameReverse(),
                companyDao.findByCriteria(SearchCriteria.builder(Company.class)
                        .sortingColumn(sortingColumn)
                        .sortingOrientation(SearchCriteria.SortingOrientation.DESCENDING)
                        .build()));
    }

    @Test
    void getTotalNumberOfFoundElementShouldReturnTheNumberOfFoundElement() {
        long numberOfFoundElement = companyDao.getTotalNumberOfFoundElementsByCriteria(SearchCriteria
                .builder(Computer.class)
                .search(Consts.Company.Apple.NAME)
                .build());
        final long expectedNumberOfFoundElement = 1;
        assertEquals(expectedNumberOfFoundElement, numberOfFoundElement);
    }

    @Test
    void getTotalNumberOfFoundElementShouldReturnZeroForNotFoundResearch() {
        long numberOfFoundElement = companyDao.getTotalNumberOfFoundElementsByCriteria(SearchCriteria
                .builder(Computer.class)
                .search("EMPTY")
                .build());
        final long expectedNumberOfFoundElement = 0;
        assertEquals(expectedNumberOfFoundElement, numberOfFoundElement);
    }
}
