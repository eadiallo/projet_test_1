package org.grorg.grordi.it;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.grorg.grordi.GrordiApplication;
import org.grorg.grordi.controller.Paths;
import org.grorg.grordi.dto.Page;
import org.grorg.grordi.dto.error.Errors;
import org.grorg.grordi.entity.Company;
import org.grorg.grordi.util.CompanyFactory;
import org.grorg.grordi.util.Consts;
import org.grorg.grordi.util.PageFactory;
import org.grorg.grordi.util.Uris;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Collection;
import java.util.Collections;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = GrordiApplication.class)
@Sql({"classpath:create_tables.sql", "classpath:insert.sql"})
class CompanyControllerIT {
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApp;

    @Autowired
    private ObjectMapper objectMapper;

    private JacksonTester<Company> companyTester;
    private JacksonTester<Page> pageTester;
    private JacksonTester<Collection> collectionTester;

    @BeforeEach
    void setup() {
        JacksonTester.initFields(this, objectMapper);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApp).build();
    }

    @Test
    void gettingExistingComputerShouldReturnIt() throws Exception {
        final String companyJson = companyTester.write(CompanyFactory.newFirstCompanyInDatabase()).getJson();

        mockMvc.perform(get(Uris.COMPANY.get(Consts.Company.Apple.ID))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(companyJson));
    }

    @Test
    void gettingNonExistingComputerShouldReturn404() throws Exception {
        mockMvc.perform(get(Uris.COMPANY.get(Consts.Company.Invalid.ID)))
                .andExpect(status().isNotFound());
    }

    @Test
    void searchingAllCompanyShouldReturnsAllCompanyPage() throws Exception {
        final String expectedPage = pageTester.write(PageFactory.getAllCompany()).getJson();
        mockMvc.perform(get(Uris.COMPANY.get())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedPage));
    }

    @Test
    void searchingCompaniesWithInvalidParameterShouldReturnErrors() throws Exception {
        final int invalidPageSize = -1;
        final String expectedError = collectionTester
                .write(Collections.singletonList(Errors.SEARCH_CRITERIA_PAGE_SIZE_SHOULD_BE_HIGHER_THAN_0))
                .getJson();
        mockMvc.perform(get(Uris.COMPANY.get())
                .param(Paths.QueryParam.PAGE_SIZE, invalidPageSize + "")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().json(expectedError));
    }

    @Test
    void deleteExistingCompanyShouldDeleteItAndReturn204() throws Exception {
        final String firstCompanyUri = Uris.COMPANY.get(1);

        mockMvc.perform(delete(firstCompanyUri))
                .andExpect(status().isNoContent());

        mockMvc.perform(get(firstCompanyUri))
                .andExpect(status().isNotFound());
    }

    @Test
    void deleteNonExistingComputerShouldReturn204() throws Exception {
        final String nonExistingCompanyUri = Uris.COMPANY.get(0);
        mockMvc.perform(delete(nonExistingCompanyUri))
                .andExpect(status().isNoContent());
    }
}
