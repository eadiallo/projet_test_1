import { Pipe, PipeTransform } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import * as moment from 'moment';
import {invalidPipeArgumentError} from '@angular/common/src/pipes/invalid_pipe_argument_error';

@Pipe({name: 'dateMoment'})
export class DateMomentPipe implements PipeTransform {
    constructor(private translate: TranslateService) {}

    /**
    *   using http://momentjs.com/docs/#/i18n/
     */
    transform(value: moment.Moment, format = 'L'): string|null {
        //
        // if (value.isValid()) {
        //    return value.locale(this.translate.currentLang).format(format);
        // } else {
        //     throw invalidPipeArgumentError(DateMomentPipe, 'Date given is not a moment format');
        // }
        return null;

    }
}
