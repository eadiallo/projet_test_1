import {AbstractControl, NG_VALIDATORS, Validator, ValidatorFn} from '@angular/forms';
import {Directive, Input} from '@angular/core';
import * as moment from 'moment';

function firstDateIsBefore(
    firstDate: moment.Moment,
    secondDate: moment.Moment,
): {[key: string]: any} {
    // condition also true when one date is null
    let isBefore = true;
    if (firstDate.isValid() && secondDate.isValid()) {
        isBefore = firstDate.isBefore(secondDate);
    }

    return !isBefore ? {'isBefore': {value: firstDate}} : null;
}

export function dateIsBefore(date: moment.Moment): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} => {
        return control.value ? firstDateIsBefore(moment(control.value, 'YYYY-MM-DD'), moment(date)) : null;
    };
}
export function dateIsAfter(date: moment.Moment): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} => {
       return control.value ? firstDateIsBefore(moment(date), moment(control.value, 'YYYY-MM-DD')) : null;
    };
}

@Directive({
    selector: '[appDateIsBefore]',
    providers: [{provide: NG_VALIDATORS, useExisting: DateIsBeforeDirective, multi: true}]
})
export class DateIsBeforeDirective implements Validator {
    @Input('appDateIsBefore') appDateIsBefore: moment.Moment;

    validate(control: AbstractControl): {[key: string]: any} {
        return this.appDateIsBefore ? dateIsBefore(this.appDateIsBefore)(control) : null;
    }
}
@Directive({
    selector: '[appDateIsAfter]',
    providers: [{provide: NG_VALIDATORS, useExisting: DateIsAfterDirective, multi: true}]
})
export class DateIsAfterDirective implements Validator {
    @Input('appDateIsAfter') appDateIsAfter: moment.Moment;

    validate(control: AbstractControl): {[key: string]: any} {
        return this.appDateIsAfter ? dateIsAfter(this.appDateIsAfter)(control) : null;
    }
}
