import { Component } from '@angular/core';
import {LoggerService} from './core/services/logger/logger.service';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {

    constructor(
        private toastr: ToastrService,
        private loggerService: LoggerService,
        private translate: TranslateService
    ) {
        translate.addLangs(['en', 'fr']);
        translate.setDefaultLang('en');
    }

}
