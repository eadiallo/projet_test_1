import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user/user.service';
import {ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    private from = '';

    constructor(
        private userService: UserService,
        private activatedRoute: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        const self = this;
        this.activatedRoute.params.subscribe((params) => {
            if (params['action'] === 'signout') {
                this.userService.signOut();
                // Redirect one the same page to update all component using userService.isLogged
                this.router.navigate(['computers']);
            } else if (params['action'] === 'navigate') {
                // TODO implement the previous url to set on from
            }
        });
    }

    signInWithGoogle() {
        this.userService.signInAndRedirectOn(this.from);
    }

}
