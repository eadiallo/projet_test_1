import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderComponent } from './header.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {httpLoaderFactory} from '../../../app.module';
import {RouterModule} from '@angular/router';
import {GoogleApiService, GoogleAuthService} from 'ng-gapi';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ HeaderComponent ],
            providers: [
                GoogleApiService,
                GoogleAuthService
            ],
            imports: [
                HttpClientModule,
                RouterModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: httpLoaderFactory,
                        deps: [HttpClient]
                    }
                }),
            ],
        })
            .compileComponents();
    }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

});
