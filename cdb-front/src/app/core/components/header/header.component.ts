import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {GoogleApiService} from 'ng-gapi';
import {UserService} from '../../services/user/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {
  constructor(
      public translate: TranslateService,
      public userService: UserService
  ) { }

    ngOnInit() {
    }
}

