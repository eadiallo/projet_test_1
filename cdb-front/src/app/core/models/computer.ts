import * as moment from 'moment';

export class Computer {

    id: number;
    name: string;
    introduced: moment.Moment;
    discontinued: moment.Moment;
    companyName = '';


    public introducedToInputFormat(): string {
       return this.dateToInputFormat(this.introduced);
    }

    public discontinuedToInputFormat(): string {
        return this.dateToInputFormat(this.discontinued);
    }

    private dateToInputFormat(date: moment.Moment): string {
        if (date != null && date.isValid()) {
            return this.introduced.format('DD-MM-YYYY');
        } else {
            return '';
        }
    }
}
