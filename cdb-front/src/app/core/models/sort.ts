export class Sort {
    constructor(public column: string, public orientation: Orientation) { }
}
export enum Orientation {
   ASC = 'ASC',
   DESC = 'DESC',
}
