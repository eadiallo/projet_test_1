import {Orientation, Sort} from './sort';

export class Page<T> {
    elements: Array<T>;
    pageIndex =  0;
    pageSize: number;
    totalPage: number;
    totalElements: number;
    sort: Sort =  new Sort('name', Orientation.ASC);
    search: string;
}
