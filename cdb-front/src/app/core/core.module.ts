import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LoggerService} from './services/logger/logger.service';
import {environment} from '../../environments/environment';
import {LoggerConfig} from './services/logger/logger.config';
import {CompanyService} from './services/company/company.service';
import {ServiceConfig} from './services/service.config';
import { NoContentComponent } from './components/no-content/no-content.component';
import {PaginationComponent} from './components/pagination/pagination.component';
import {ErrorComponent} from './components/error/error.component';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        NoContentComponent,
        PaginationComponent,
        ErrorComponent
    ],
    providers: [
        LoggerService,
        {provide: LoggerConfig, useValue: environment.loggerConfig},
        CompanyService,
        {provide: ServiceConfig, useValue: environment.serviceConfig}
    ],
    exports: [
        PaginationComponent,
        ErrorComponent
    ]
})
export class CoreModule { }
