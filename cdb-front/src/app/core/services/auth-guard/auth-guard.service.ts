import {Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, NavigationEnd, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {UserService} from '../user/user.service';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {Role} from '../../models/role';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

    constructor(
        private userService: UserService,
        private router: Router,
        private toastr: ToastrService,
        private translate: TranslateService
    ) {}

    canActivate(route: ActivatedRouteSnapshot): Observable<boolean>|Promise<boolean>|boolean {

        // First check if user is connected
        if (!this.userService.isLogged()) {
            this.translate.get('error.401', {}).subscribe((res: string) => {
                this.toastr.error(res);
            });
            this.router.navigate(['login', 'navigate'], {replaceUrl: true});
        }

        // Now check if the user has the role to do
        const roles = route.data.roles as Role[];
        if (roles) {
            roles.forEach(role => {
                if (!this.userService.hasRole(role)) {
                    this.translate.get('error.403', {}).subscribe((res: string) => {
                        this.toastr.error(res);
                    });
                    return false;
                }
            });

        }
        return true;
    }
}
