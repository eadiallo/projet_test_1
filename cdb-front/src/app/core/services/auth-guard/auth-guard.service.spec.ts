import { TestBed, inject } from '@angular/core/testing';
import {AuthGuardService} from './auth-guard.service';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {httpLoaderFactory} from '../../../app.module';
import {RouterModule} from '@angular/router';
import {GoogleApiService, GoogleAuthService} from 'ng-gapi';


describe('AuthGuardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthGuardService, GoogleAuthService, GoogleApiService],
        imports: [
            HttpClientModule,
            RouterModule,
            TranslateModule.forRoot({
                loader: {
                    provide: TranslateLoader,
                    useFactory: httpLoaderFactory,
                    deps: [HttpClient]
                }
            }),
        ],
    });
  });

});
