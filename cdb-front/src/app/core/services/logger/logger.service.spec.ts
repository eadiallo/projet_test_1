import { TestBed, inject } from '@angular/core/testing';

import { LoggerService } from './logger.service';
import {LoggerConfig} from './logger.config';
import {LogLevel} from './log.level';

describe('Test the LoggerService in debug mode.', () => {

    let loggerService: LoggerService;
    let loggerConfig: LoggerConfig = { level: LogLevel.DEBUG};

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [LoggerService, {provide: LoggerConfig, useValue: loggerConfig}]
        });
        loggerService = TestBed.get(LoggerService);
        loggerConfig = TestBed.get(LoggerConfig);
    });

    it('should be created', () => {
        expect(loggerService).toBeTruthy();
    });

    it('Should print info log in debug level', () => {
            spyOn(console, 'info');
            loggerService.info('info');
            expect(console.info).toHaveBeenCalled();
    });

    it('Should print debug log when we are in debug level', () => {
        spyOn(console, 'debug');
        loggerService.debug('debug');
        expect(console.debug).toHaveBeenCalled();
    });
});

describe('Test the LoggerService in info mode.', () => {
    let loggerService: LoggerService;
    let loggerConfig: LoggerConfig = { level: LogLevel.INFO};
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [LoggerService, {provide: LoggerConfig, useValue: loggerConfig}]
        });
        loggerService = TestBed.get(LoggerService);
        loggerConfig = TestBed.get(LoggerConfig);
    });

    it('Should log error when we are in info level', () => {
        spyOn(console, 'error');
        loggerService.error('info');
        expect(console.error).toHaveBeenCalled();
    });

    it('Should log info when we are in info level', () => {
        spyOn(console, 'info');
        loggerService.info('info');
        expect(console.info).toHaveBeenCalled();
    });
});
