import {Injectable} from '@angular/core';
import {LogLevel} from './log.level';

@Injectable({
    providedIn: 'root'
})
export class LoggerConfig {

    public readonly level: LogLevel;
    constructor() {
    }

}
