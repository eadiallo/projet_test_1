import {Computer} from '../../models/computer';

export class ComputerDTO {

    public company: string;
    public id: number;
    public name: string;
    public introduced: string;
    public discontinued: string;

    constructor() {
        this.company = '';
    }

    static fromComputer(computer: Computer): ComputerDTO {
        const computerMapper = new ComputerDTO();
        computerMapper.company = computer.companyName || '';
        computerMapper.id = computer.id;
        computerMapper.name = computer.name;
        if (computer.introduced === undefined || !computer.introduced.isValid()) {
            computerMapper.introduced = null;
        } else {
            computerMapper.introduced = computer.introduced.format('YYYY-MM-DD');
        }

        if (computer.discontinued === undefined || !computer.discontinued.isValid()) {
            computerMapper.discontinued = null;
        } else {
            computerMapper.discontinued = computer.discontinued.format('YYYY-MM-DD');
        }

        return computerMapper;
    }

}
