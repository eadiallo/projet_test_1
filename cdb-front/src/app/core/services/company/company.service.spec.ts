import { TestBed } from '@angular/core/testing';

import { CompanyService } from './company.service';
import {LoggerService} from '../logger/logger.service';
import {LogLevel} from '../logger/log.level';
import {LoggerConfig} from '../logger/logger.config';
import {BrowserModule} from '@angular/platform-browser';
import {ToastrModule} from 'ngx-toastr';
import {CoreModule} from '../../core.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {CompanyServiceMock} from './company.service.mock';
import {ServiceConfig} from '../service.config';
import {CompanyMapper} from './company.mapper';

describe('CompanyService', () => {

    let loggerService: LoggerService;
    let loggerConfig: LoggerConfig = { level: LogLevel.DEBUG};
    let serviceConfig: ServiceConfig = { restUrl: 'localhost:8080/'};
    let companyService: CompanyService;

    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [LoggerService, CompanyService,
                {provide: LoggerConfig, useValue: loggerConfig},
                {provide: LoggerService, useValue: loggerService},
                {provide: ServiceConfig, useValue: serviceConfig},
            ],
            imports: [
                BrowserModule,
                CoreModule,
                HttpClientTestingModule,
                BrowserAnimationsModule,
                ToastrModule.forRoot()
            ],
        });
        loggerService = TestBed.get(LoggerService);
        loggerConfig = TestBed.get(LoggerConfig);
        companyService = TestBed.get(CompanyService);
        httpMock = TestBed.get(HttpTestingController);
        serviceConfig = TestBed.get(ServiceConfig);

    });


    it('Should a page company list', () => {

        const companiesPage = CompanyServiceMock.generateCompanyPage();

        companyService.fetchPage({pageSize: companiesPage.pageSize})
            .subscribe(companies => {
                expect(companies).toBe(companiesPage);
        });

        const requestMocked = httpMock.expectOne(
            re => re.url === CompanyServiceMock.buildUrl(serviceConfig.restUrl)
        );
        requestMocked.flush(companiesPage);

    });

    it('Should recover the exact same company', () => {
        const dummyCompany = CompanyServiceMock.generateCompany();

        companyService.getById(dummyCompany.id).subscribe(company => {
            expect(company).toEqual(dummyCompany);
        });

        const requestMocked = httpMock.expectOne(
            {method: 'GET', url: CompanyServiceMock.buildUrl(
                serviceConfig.restUrl, String(dummyCompany.id)
            )}
        );
        requestMocked.flush(dummyCompany as CompanyMapper);
    });

    afterEach(() => {
        httpMock.verify();
    });


});
