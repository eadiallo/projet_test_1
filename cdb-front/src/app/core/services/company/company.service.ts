import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ServiceConfig} from '../service.config';
import {LoggerService} from '../logger/logger.service';
import {Company} from '../../models/company';
import {GenericService} from '../generic.service';
import {CompanyMapper} from './company.mapper';

@Injectable({
  providedIn: 'root'
})
export class CompanyService extends GenericService<Company, CompanyMapper> {

    public static readonly PATH = 'companies';
    constructor(
        private logger: LoggerService,
        serviceConfig: ServiceConfig,
        httpClient: HttpClient
    ) {
        super(httpClient, serviceConfig);
    }

    public parse(mapper: CompanyMapper): Company {
        const company = new Company();
        company.id = mapper.id;
        company.name = mapper.name;
        return company;
    }

    getPath(): string {
        return CompanyService.PATH;
    }
}
