import * as faker from 'faker';
import {Company} from '../../models/company';
import {CompanyService} from './company.service';
import {Page} from '../../models/page';
import {Orientation, Sort} from '../../models/sort';

export class CompanyServiceMock {


    public static generateCompany(): Company {
        const company: Company = new Company();
        company.id = faker.random.number(100);
        company.name = faker.company.companyName();

        return company;
    }

    public static generateCompanyList(size: number): Company[] {
        const companies: Company[] = [];

        for (let c = 0; c < size; c++) {
            companies.push(CompanyServiceMock.generateCompany());
        }

        return companies;
    }

    public static generateCompanyPage(): Page<Company> {
        return {
            pageIndex: 0,
            pageSize: 20,
            totalPage: faker.random.number(100),
            elements: this.generateCompanyList(20),
            totalElements: faker.random.number(333),
            sort: new Sort('column', Orientation.ASC),
            search: ''
        };
    }

    public static buildUrl(restUrl, pathArg = ''): string {
        return restUrl + '/' + CompanyService.PATH + '/' + pathArg;
    }

}
