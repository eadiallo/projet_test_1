import {Injectable, NgZone} from '@angular/core';
import GoogleUser = gapi.auth2.GoogleUser;
import {GoogleAuthService} from 'ng-gapi';
import {Router} from '@angular/router';
import AuthResponse = gapi.auth2.AuthResponse;
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {Role} from '../../models/role';
import {HttpClient} from '@angular/common/http';
import {ServiceConfig} from '../service.config';

@Injectable({
  providedIn: 'root'
})
export class UserService {

    static readonly PATH_TO_API = 'current/user';

    public static LOCAL_STORAGE_ID_TOKEN = 'idToken';
    private googleUser: GoogleUser;
    private roles: Role[] = [];
    public readonly ROLE: typeof Role = Role;


    constructor(
        private googleAuth: GoogleAuthService,
        private router: Router,
        private ngZone: NgZone,
        private translate: TranslateService,
        private toastr: ToastrService,
        private httpClient: HttpClient,
        public serviceConfig: ServiceConfig
    ) {
        if (UserService.getIdToken()) {
            this.fetchRole();
        }
    }


    public static getIdToken(): string {
        return localStorage.getItem(UserService.LOCAL_STORAGE_ID_TOKEN);
    }


    private fetchRole(): void {
        this.httpClient.get<Role[]>(this.serviceConfig.restUrl + '/' + UserService.PATH_TO_API).subscribe(res => {
            this.roles = [];
            if (res['roles']) {
                Object.keys(Role).filter(role => {
                    if (!isNaN(Number(Role[role]))) {
                        for (let r = 0; r < res['roles'].length; r++) {
                            if (role === res['roles'][r]) {
                                this.roles.push(Role[role]);
                            }
                        }
                    }
                });
            }
    });
}

    public hasRole(role: Role): boolean {
        return this.roles.indexOf(role) !== -1;
    }

    public signIn(): void {
        this.signInAndRedirectOn('computers');
    }

    public signInAndRedirectOn(path: string): void {
        this.googleAuth.getAuth()
            .subscribe((auth) => {
                auth.signIn().then(res => {
                        this.signInSuccessHandler(res);
                        // Force the component computers to load when we navigate on it
                        // because with lazy loading and naviguate, it doesn't work everytime
                        // using ngZone fix it
                        // src: https://stackoverflow.com/questions/41381421/angular-2-view-not-responding-after-navigate
                        this.ngZone.run(() => {
                            this.translate.get('login.success', {}).subscribe((trans: string) => {
                                this.toastr.success(trans);
                                this.fetchRole();
                                this.router.navigate([path]);
                            });
                        });
                    }
                );
            });

    }

    public signOut(): void {
        this.googleAuth.getAuth().subscribe((auth) => {
            localStorage.removeItem(UserService.LOCAL_STORAGE_ID_TOKEN);
            this.googleUser = null;
            this.roles = [];
            auth.signOut();
        });
    }

    public isLogged(): boolean {
        // Try to load user from storage if avalaible
        return (this.googleUser != null && this.googleUser.isSignedIn()) ||
            localStorage.getItem(UserService.LOCAL_STORAGE_ID_TOKEN) != null;
    }

    private signInSuccessHandler(res: GoogleUser) {
        this.googleUser = res;
        this.saveTokenToStorage(res.getAuthResponse());
    }

    private saveTokenToStorage(authResponse: AuthResponse): void {
        localStorage.setItem(
            UserService.LOCAL_STORAGE_ID_TOKEN, authResponse.id_token
        );
    }
}
