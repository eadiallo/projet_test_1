import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ServiceConfig {
    public readonly restUrl: string;
    constructor() {
    }
}
