import {Routes} from '@angular/router';
import {NoContentComponent} from './core/components/no-content/no-content.component';
import {LoginComponent} from './core/components/login/login.component';

export const ROUTES: Routes = [
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'login/:action',
        component: LoginComponent
    },
    {
        path: 'login/:action/:route',
        component: LoginComponent
    },
    {
        path: '',
        redirectTo: '/computers',
        pathMatch: 'full'
    },
    { path: '**', component: NoContentComponent }
];
