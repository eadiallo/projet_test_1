import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {RouterModule} from '@angular/router';
import {CoreModule} from '../../core/core.module';
import {ComputerPageComponent} from './computer-page/computer-page.component';
import {ComputerEditComponent} from './computer-edit/computer-edit.component';
import {ComputerEditResolver} from './computer-edit/computer-edit.resolver';
import {ComputerService} from '../../core/services/computer/computer.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DateIsAfterDirective, DateIsBeforeDirective} from '../../directive/date.directive';
import {TranslateModule} from '@ngx-translate/core';
import {ComputerAddComponent} from './computer-add/computer-add.component';
import {AuthGuardService} from '../../core/services/auth-guard/auth-guard.service';
import {Role} from '../../core/models/role';
import {ErrorComponent} from '../../core/components/error/error.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        CoreModule,
        TranslateModule.forChild({}),
        RouterModule.forChild([
            {
                path: '',
                component: ComputerPageComponent
            },
            {
                path: 'add',
                component: ComputerAddComponent
            },
            {
                path: ':id',
                component: ComputerEditComponent,
                canActivate: [ AuthGuardService ],
                data: {roles: [Role.ADMIN]},
                resolve: {
                    computer: ComputerEditResolver
                }
            }
        ]),
    ],
    declarations: [
        ComputerPageComponent,
        ComputerEditComponent,
        ComputerAddComponent,
        DateIsBeforeDirective,
        DateIsAfterDirective,
    ],
    providers: [
        ComputerService,
        ComputerEditResolver
    ]
})
export class ComputerModule { }
