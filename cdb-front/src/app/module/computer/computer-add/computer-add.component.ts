import { Component, OnInit } from '@angular/core';
import {Company} from '../../../core/models/company';
import {ToastrService} from 'ngx-toastr';
import {ComputerService} from '../../../core/services/computer/computer.service';
import {ComputerDTO} from '../../../core/services/computer/computerDTO';
import {Router} from '@angular/router';
import {CompanyService} from '../../../core/services/company/company.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-computer-add',
  templateUrl: './computer-add.component.html',
  styleUrls: ['./computer-add.component.scss']
})
export class ComputerAddComponent implements OnInit {

    errorsFromServer: string[] = [];

    computer: ComputerDTO;
    companies: Company[];

    constructor(
        private computerService: ComputerService,
        private companyService: CompanyService,
        private router: Router,
        private toast: ToastrService,
        private translate: TranslateService
    ) {
        this.computer = new ComputerDTO();
    }

    ngOnInit() {
        this.companyService.fetchPage({pageIndex: 0, pageSize: 70}).subscribe(companyPage => {
            this.companies = companyPage.elements;
        }, error => {
            this.toast.error('Server seems to be unreachable');
        });
    }

    public add(): void {
        this.computerService.post(this.computer).subscribe(res => {
            this.translate.get('computer.add.success', {}).subscribe((trans: string) => {
                this.toast.success(trans);
            });
            this.router.navigateByUrl('/computers');
        }, error => {
            this.translate.get('computer.add.error', {}).subscribe((trans: string) => {
                this.toast.error(trans);
            });
        });
    }


}
