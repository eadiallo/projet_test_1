import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComputerPageComponent } from './computer-page.component';
import {environment} from '../../../../environments/environment';
import {CompanyPageComponent} from '../../company/company-page/company-page.component';
import {ServiceConfig} from '../../../core/services/service.config';
import {CompanyService} from '../../../core/services/company/company.service';
import {PaginationComponent} from '../../../core/components/pagination/pagination.component';
import {ComputerService} from '../../../core/services/computer/computer.service';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {ToastrModule} from 'ngx-toastr';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {httpLoaderFactory} from '../../../app.module';
import {RouterModule} from '@angular/router';
import {GoogleApiService, GoogleAuthService} from 'ng-gapi';

describe('ComputersComponent', () => {
  let component: ComputerPageComponent;
  let fixture: ComponentFixture<ComputerPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [
            ComputerPageComponent,
            PaginationComponent
        ],
        providers: [
            ComputerService,
            {provide: ServiceConfig, useValue: environment.serviceConfig},
            GoogleAuthService,
            GoogleApiService
        ],
        imports: [
            HttpClientModule,
            RouterModule,
            ToastrModule.forRoot({
                positionClass: 'toast-bottom-right',
                preventDuplicates: true,
            }),
            TranslateModule.forRoot({
                loader: {
                    provide: TranslateLoader,
                    useFactory: httpLoaderFactory,
                    deps: [HttpClient]
                }
            }),
        ]
    })
        .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComputerPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

});
