import {Component, ElementRef, NgZone, OnInit, ViewChild} from '@angular/core';
import {Page} from '../../../core/models/page';
import {Computer} from '../../../core/models/computer';
import {ComputerService} from '../../../core/services/computer/computer.service';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {Orientation, Sort} from '../../../core/models/sort';
import {UserService} from '../../../core/services/user/user.service';

@Component({
    selector: 'app-computer-page',
    templateUrl: './computer-page.component.html',
    styleUrls: ['./computer-page.component.scss']
})

export class ComputerPageComponent implements OnInit {

    @ViewChild('searchComputer') searchComputerInput: ElementRef;

    public currentPage: Page<Computer>;
    private lastSearch = '';
    public sortArrowsMap = new Map()
        .set('name', '')
        .set('company', '')
        .set('introduced', '')
        .set('discontinued', '');
    private sort: Sort;
    private pageSize = 10;

    constructor(
        private computerService: ComputerService,
        private toast: ToastrService,
        public translate: TranslateService,
        public userService: UserService
    ) {
    }

    ngOnInit() {
    }

    public deleteComputer(id: number): void {
        this.computerService.deleteById(id).subscribe(res => {
            this.translate.get('computer.delete.success', {}).subscribe((success: string) => {
                this.toast.success(success);
            });
            this.updatePage(this.currentPage.pageIndex);
        }, error => {
            this.translate.get('computer.delete.error', {}).subscribe((errorMessage: string) => {
                this.toast.error(errorMessage);
            });
        });
    }

    public updatePage(pageIndex: number): void {
        this.computerService.fetchPage(
            {
                pageIndex: pageIndex,
                pageSize: this.pageSize,
                search: this.lastSearch,
                sort: this.sort
            }).subscribe(computerPage => {
            this.currentPage = computerPage;
        }, error => {
            this.toast.error('Server seems to be unreachable');
        });

    }

    pageSizeChanged(pageSize: number) {
        this.pageSize = pageSize;
        this.updatePage(0);
    }

    public searchKeyUp(event) {
        this.lastSearch = this.searchComputerInput.nativeElement.value;
        this.sort = this.currentPage.sort;
        this.updatePage(0);
    }

    sortByColumnName(event) {

        if (this.currentPage.sort.column !== event) {
            this.updateOrientation(event, Orientation.ASC, '-up');
        } else {
            if (this.currentPage.sort.orientation === Orientation.ASC) {
                this.updateOrientation(event, Orientation.DESC, '-down');
            } else {
                this.updateOrientation(event, Orientation.ASC, '-up');

            }
        }
    }

    updateOrientation(column, orientation, arrow) {
        this.sort = new Sort(column, orientation);
        this.updatePage(this.currentPage.pageIndex);
        for (const sign of Array.from(this.sortArrowsMap.keys())) {
            this.sortArrowsMap.set(sign, '');
        }
        this.sortArrowsMap.set(column, arrow);
    }
}
