import {Computer} from '../../../core/models/computer';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {ComputerService} from '../../../core/services/computer/computer.service';
import {Observable} from 'rxjs';
import * as moment from 'moment';

@Injectable()
export class ComputerEditResolver implements Resolve<Computer> {

    constructor(private computerService: ComputerService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Computer> {
        if (route.paramMap.get('name')) {
            return new Observable<Computer>(observer => {
                const computer = new Computer();
                computer.name = route.paramMap.get('name');
                computer.id = parseInt(route.paramMap.get('id'), 10);
                if (route.paramMap.get('introduced') != null ) {
                    computer.introduced = moment.unix(parseInt(route.paramMap.get('introduced'), 10) / 1000);
                }
                computer.discontinued = moment.unix(parseInt(route.paramMap.get('discontinued'), 10) / 1000);
                observer.next(computer);
                observer.complete();
            });
        } else {
            return this.computerService.get({pathArg: route.paramMap.get('id')}) as Observable<Computer>;
        }
    }
}
