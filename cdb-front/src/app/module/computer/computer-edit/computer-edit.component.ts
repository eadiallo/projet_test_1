import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ComputerService} from '../../../core/services/computer/computer.service';
import {TranslateService} from '@ngx-translate/core';
import {ComputerDTO} from '../../../core/services/computer/computerDTO';
import {ToastrService} from 'ngx-toastr';

@Component({
    selector: 'app-computer-edit',
    templateUrl: './computer-edit.component.html',
    styleUrls: ['./computer-edit.component.scss']
})
export class ComputerEditComponent implements OnInit {

    public errorsFromServer: string[] = [];

    public computer: ComputerDTO;
    @ViewChild('computerForm') computerForm;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private computerService: ComputerService,
        private translate: TranslateService,
        private toastr: ToastrService
    ) { }

    ngOnInit() {
        this.computer =  ComputerDTO.fromComputer(this.route.snapshot.data['computer']);
    }

    update() {
        this.errorsFromServer = [];

        // We should first fetch the id of the company
        this.computerService.getById(this.computer.id).subscribe(computer => {
            this.computer['company'] = computer.companyName;
            this.computerService.put(this.computer)
                .subscribe(res => {
                    this.translate.get('computer.edit.success', {}).subscribe( (success: string) => {
                        this.toastr.success(success);
                        this.router.navigate(['computers']);
                    });
                }, error => {
                    if (error.status === 400) {
                        this.translate.get('error.400', {}).subscribe((res: string) => {
                            this.toastr.error(res);
                        });
                    }
                });
        });

    }
}
