import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CompanyPageComponent} from './company-page/company-page.component';
import {RouterModule} from '@angular/router';
import {CoreModule} from '../../core/core.module';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
    imports: [
        CommonModule,
        CoreModule,
        TranslateModule.forChild({}),
        RouterModule.forChild([
                {
                    path: '',
                    component: CompanyPageComponent
                },
            ]
        )
    ],
    declarations: [
        CompanyPageComponent
    ]
})
export class CompanyModule { }
